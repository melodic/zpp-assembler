import numpy as np


def MSE(results: np.ndarray, grand_truth: np.ndarray) -> float:
    diffs = results - grand_truth
    diffs = np.square(diffs)
    return sum(diffs) / len(diffs)


def MSLE(results: np.ndarray, grand_truth: np.ndarray) -> float:
    diffs = np.log(results + 1) - np.log(grand_truth + 1)
    diffs = np.square(diffs)
    return sum(diffs) / len(diffs)
