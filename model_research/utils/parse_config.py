import os

from utils.parse_yaml import parse
from definitions import \
    DQN_EXPERIMENT_CONFIGS_DIR, \
    PPO_EXPERIMENT_CONFIGS_DIR, \
    A2C_EXPERIMENT_CONFIGS_DIR, \
    DDPG_EXPERIMENT_CONFIGS_DIR, \
    SAC_EXPERIMENT_CONFIGS_DIR


def parse_dqn_config(file):
    return parse(os.path.join(DQN_EXPERIMENT_CONFIGS_DIR, file))


def parse_ppo_config(file):
    return parse(os.path.join(PPO_EXPERIMENT_CONFIGS_DIR, file))


def parse_a2c_config(file):
    return parse(os.path.join(A2C_EXPERIMENT_CONFIGS_DIR, file))


def parse_ddpg_config(file):
    return parse(os.path.join(DDPG_EXPERIMENT_CONFIGS_DIR, file))


def parse_sac_config(file):
    return parse(os.path.join(SAC_EXPERIMENT_CONFIGS_DIR, file))
