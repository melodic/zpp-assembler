import numpy
import os
import json

from definitions import ML_MODELS_DIR
from stable_baselines3.common.save_util import save_to_zip_file, load_from_zip_file


def export(filename: str, model):
    path = os.path.join(ML_MODELS_DIR, filename)
    save_to_zip_file(path, model.get_parameters())


def load(filename: str):
    path = os.path.join(ML_MODELS_DIR, filename)
    result = load_from_zip_file(path)
    return result[0]


def DQN_from_zip(filename, env, model_config):
    path = os.path.join(ML_MODELS_DIR, filename)
    parameters = load_from_zip_file(path)
    model = DQN('MlpPolicy',
                env,
                verbose=1,
                batch_size=model_config['batch_size'],
                gamma=model_config['gamma'],
                learning_rate=model_config['learning_rate'])
    model.load_parameters(parameters)
    return model
