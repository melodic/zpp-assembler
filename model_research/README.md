# Ensembling model research
This directory contains files associated with the ensembling model development process. The model is based on reinforcement learning.

## Directory contents
- `src/env/`: implementation of the OpenAI Gym environment utilized by the model
- `src/experiments/`: code and configurations associated with model experiments
- `src/logging/`: code associated with logging experiment data to Neptune.ai
- `main.py`: program for running experiments from `src/experiments`
- `definitions.py`: utility constans, such as directory paths

## Installation
In project root directory:
```
python3 -m venv venv
source ./venv/bin/activate
pip3 install -r ./requirements.txt --no-cache-dir
```

## Experiment setup
1. Create a `neptune_key.yaml` file with a single line: `API_KEY: <your_neptune_ai_api_token>`.
2. Insert a CSV file with training data, named `predictions.csv` in the `data/external/` directory.
3. Choose which experiments you wish to run by uncommenting appropriate lines in the `main.py` program.
4. Configure the experiments chosen above by modifying the `.yaml` files associated with the experiments, e.g. `src/experiments/ppo/configs/continuous_reward.yaml`. 

## Experiment startup
Simply run the `main.py` program via:
```
python3 main.py
```
