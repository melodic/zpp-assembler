class EnsemblerEnvConfig:
    """
    A class containing information about training data structure used by EnsemblerEnv.

    Attributes
    ----------
        input_size : int
            A number of forecasters.
        window_size : int
            The number of past predictions used for computing state by agent. Environment
            uses this value for creating sample initial state at the beginning of each episode.
        episode_length : int
            The number of states during each episode.
        max_prediction_value : float
            Upper bound of prediction value. Used b environment for creating sample initial
            state at the beginning of each episode.
        first_prediction_column : int
            The index of the first column of data that stores predictions for every time step.
        grand_truth_column : int
            The index of the column of data that stores grand truth for every time step.
        mode : str
            Defines action space. One of the following: 'discrete' or 'continuous'. 'discrete' mode
            implies action space to be an instance of Discrete class. 'continuous' mode implies
            action space to be an instance of Box class.
        extend_predictions_by : float
            Defines the gap between values of additional (mocked) forecasters and the bounds of
            initial set of predictions. Setting the value to 0, disables creating additional
            mocked forecasters.
        reward_diff_factor : float
            The contribution of the difference between the best forecaster's prediction and ensembler's
            prediction to the reward.
        extended_state : bool
            Is environment using extended state.
    """
    def __init__(self, input_size: int = 6, window_size: int = 10, episode_length: int = 100,
                 max_prediction_value: float = 100.0, first_prediction_column: int = 2,
                 grand_truth_column: int = 1, mode: str = 'continuous',
                 extend_predictions_by: float = 10.0, reward_diff_factor: float = 0.05,
                 extended_state: bool = False):
        self.input_size: int = input_size
        self.window_size: int = window_size
        self.episode_length: int = episode_length
        self.max_prediction_value: float = max_prediction_value
        self.first_prediction_column: int = first_prediction_column
        self.grand_truth_column: int = grand_truth_column
        self.mode: str = mode
        self.extend_predictions_by = extend_predictions_by
        self.reward_diff_factor = reward_diff_factor
        self.extended_state = extended_state
