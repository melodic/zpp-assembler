import gym
import random

import numpy as np

from gym import spaces
from src.env.EnsemblerEnvConfig import EnsemblerEnvConfig
from typing import Tuple


class EnsemblerEnv(gym.Env):
    """
    An environment developed to train RL algorithms dedicated to deal with time series.
    Provides two modes: 'discrete' and 'continuous' that define the kind of action space.


    Parameters
    ----------
    data : numpy.ndarray
        A set of predictions, computed by enseblators, used as training data. It must be
        stored in some csv file.
    config : EnsemblerEnvConfig
        A class containing information about training data structure.

    Attributes
    ----------
    config : EnsemblerEnvConfig
        A class containing information about training data structure.
    mode : str
        Whether environment must use discrete or continuous action_space. In 'discrete' mode
        action_space is a Discrete Object, and in 'continuous' mode it's a Box object.
    data_predictions : numpy.ndarray
        A set of predictions computed by forecasters, used as training data (including grand
        truth values).
    data_y : numpy.ndarray
        Grand truth values for each training example.
    current_y_val : float
        Grand truth value for the current training example.
    current_episode_len : int
        Number of steps performed yet in the episode. When this is equal to the length
        of the episode, the environment resets.
    total_step_num : int
        Number of all steps i.e. the number of training examples in all epochs.
    current_base_step : int
        Index in the data_predictions array of the beginning of the currently processed training
        example.
    current_prediction : float
        Overall prediction value of the learning agent at the current time step. The environment
        is aware of method used by agent to compute predictions. This value is used for computing
        current_reward as it depends on several most recent past predictions, and its current
        prediction.
    current_reward : float
        A reward computed for given state followed by indicated action at the current time step.
    current_input_forecasts : numpy.ndarray
        Predictions of forecasters used by the learning agent as an input. They are used by the
        environment (alongside with state and action) to compute current_prediction, and also to
        compute current_reward.
    current_state : numpy.ndarray
        A given current state. For predefined window size of validation set that corresponds to the
        previous values of the time-series until current time, it consists of past predictions of
        those values.
    prev_error : float
        The Latest prediction error for run logging purposes.
    metadata : str
        A mode of the Gym environment. Describes how to return and present computation data. In this
        case it is set to 'human' which forces the environment to render to the current display or
        terminal and return nothing.
    observation_space : gym.spaces.space.Space
        Defines valid state values i.e. an array of past (and current one) predictions. It's used for
        sampling past state for the first step of the episode.
    action_space : gym.spaces.space.Space
        Defines valid action values i.e. weights assigned to forecasters.
    additional_mocked_data : np.ndarray
        Predictions of two mocked forecasters.
    mock_additional_forecasters : bool
        Whether a model should use additional mocked forecasters.
    """

    def __init__(self, data: np.ndarray, config: EnsemblerEnvConfig = EnsemblerEnvConfig()):
        super(EnsemblerEnv, self).__init__()
        self.config: EnsemblerEnvConfig = config
        self.mode: str = config.mode
        self.data_predictions: np.ndarray = \
            (data[:, self.config.first_prediction_column:self.config.first_prediction_column + self.config.input_size]).astype(float)
        self.mock_additional_forecasters = False
        self.additional_mocked_data = []
        self._mock_additional_forecasters()
        if self.config.extended_state:
            self.observation_space: spaces.space.Space = spaces.Box(
                low=0.0,
                high=self.config.max_prediction_value,
                shape=(self.config.window_size, 4))
        else:
            self.observation_space: spaces.space.Space = spaces.Box(
                low=0.0,
                high=self.config.max_prediction_value,
                shape=(self.config.window_size,))
        if self.mode == 'continuous':
            self.action_space: spaces.space.Space = spaces.Box(
                low=0.0,
                high=1.0,
                shape=(self.config.input_size + (2 if self.mock_additional_forecasters else 0),))
        elif self.mode == 'discrete':
            self.action_space: spaces.space.Space = spaces.Discrete(
                2 ** (self.config.input_size + (2 if self.mock_additional_forecasters else 0)) - 1)
        self.current_episode_len: int = 0
        self.current_prediction: float = 0
        self.current_reward: float = 0
        self.current_error: float = 0
        self.current_input_forecasts: np.ndarray = np.ndarray([])
        self.current_state: np.ndarray = np.ndarray([])
        if self.config.extended_state:
            self.delay = 5
        else:
            self.delay = 0
        self.prev_error: float = 0
        self.last_predictions: float = 0
        self.metadata: object = {'render.modes': ['human']}
        self.data_y: np.ndarray = (data[:, self.config.grand_truth_column]).astype(float)
        self.total_step_num: int = len(data)
        self.current_base_step: int = 0
        self.current_y_val: float = 0
        self.current_y_val_true: float = 0
        self.current_index: int = 0
        self.current_forecasters_errors: np.ndarray = np.ndarray([])
        self.hole_fill_current_value: float

    def _is_value_set(self, value):
        return not np.isnan(value)

    def _load_current_step_data(self):
        """
        Extracts data specific to the current time step from the general data store.
        """
        index: int = (self.current_base_step + self.current_episode_len) % self.total_step_num
        self.current_index = index
        self.current_y_val_true = self.data_y[index]

        if self.mock_additional_forecasters:
            self.current_input_forecasts = np.concatenate(
                (self.data_predictions[index], self.additional_mocked_data[index]), axis=0)
        else:
            self.current_input_forecasts = self.data_predictions[index]

        self._compute_hole_fill_value()
        for i in range(self.data_predictions.shape[1]):
            if not self._is_value_set(self.current_input_forecasts[i]):
                self.current_input_forecasts[i] = self.hole_fill_current_value

    def _compute_hole_fill_value(self):
        forecasters = self.data_predictions.shape[1]
        forecasters_online: int = 0
        forecasters_sum: float = 0.

        for i in range(forecasters):
            if self._is_value_set(self.current_input_forecasts[i]):
                forecasters_online += 1
                forecasters_sum += self.current_input_forecasts[i]

        if forecasters_online == 0:
            self.hole_fill_current_value = np.random.random_sample() * 100
        else:
            self.hole_fill_current_value = forecasters_sum / forecasters_online

    def _mock_additional_forecasters(self):
        """
        Adds two additional dummy forecasters, which broaden set of possible ensemble output
        values. First additional forecaster shall have output value greater than the greatest
        predicted value per timestamp. The second forecaster, analogously, should have
        the lesser value. The exact values are defined by config.extend_predictions_by .
        If This value is equal 0, a model will not use additional forecasters, set
        mock_additional_forecasters to False and not fill additional_mocked_data array.
        """
        if self.config.extend_predictions_by == 0.:
            self.mock_additional_forecasters = False
        else:
            self.mock_additional_forecasters = True
            self.additional_mocked_data = np.zeros([self.data_predictions.shape[0], 2], dtype=float)
            for i in range(self.data_predictions.shape[0]):
                self.additional_mocked_data[i, 0] = \
                    np.max(self.data_predictions[i]) + self.config.extend_predictions_by
                self.additional_mocked_data[i, 1] = \
                    np.min(self.data_predictions[i]) - self.config.extend_predictions_by

                self.additional_mocked_data[i, 0] =\
                    min(self.additional_mocked_data[i, 0], self.config.max_prediction_value)
                self.additional_mocked_data[i, 1] = max(self.additional_mocked_data[i, 1], 0.)

    def _proceed_to_next_state(self):
        """
        Updates the past predictions list in respect to the window size. Removes the
        oldest prediction and appends the one for the current timestamp.
        """
        if self.config.extended_state:
            for i in range(self.config.window_size - 1):
                self.current_state[i][3] = self.current_state[i + 1][3]

            self.current_state[self.config.window_size - 1][3] = self.current_prediction
            self.delay += 1
        else:
            self.current_state[:-1] = self.current_state[1:]  # pops
            self.current_state[-1] = self.current_prediction  # appends

    def _action_to_forecaster_selection(self, action: int) -> np.ndarray:
        """
        Converts selected action to the corresponding forecasters' selection. Used exclusively
        in 'discrete' mode.

        Parameters
        ----------
        action : int
            Discrete action.

        Returns
        -------
        selection : np.ndarray
            An array of zeros and ones indicating which forecaster should be considered or not while
            computing the next state and reward.
        """
        binary_str: str = np.binary_repr(action)
        while len(binary_str) < self.config.input_size + (2 if self.mock_additional_forecasters else 0):
            binary_str = '0' + binary_str
        selection = np.array([x for x in binary_str], dtype=int)
        return selection

    def _take_action(self, action):
        """
        Produces a weighted mean of the forecaster predictions. If all the weights are
        equal to 0, then the most recent prediction is treated as the current one.

        Parameters:
        ----------
        action : np.ndarray
            An array of weights corresponding to forecasters.
        """
        if self.mode == 'continuous':
            weight_sum: float = np.sum(action)
            if weight_sum > 0:
                self.current_prediction = np.sum((action / weight_sum) * self.current_input_forecasts)
                """
                if self.config.extended_state:
                    if self.delay == 0:
                        self.current_prediction = (
                            np.sum((action / weight_sum) * self.current_input_forecasts),
                            self.current_y_val,
                            np.sum((action / weight_sum) * self.current_input_forecasts) - self.current_y_val,
                            np.sum((action / weight_sum) * self.current_input_forecasts))
                    else:
                        self.current_prediction = (
                            self.current_state[self.config.window_size - self.delay][3],
                            self.current_y_val,
                            self.current_state[self.config.window_size - self.delay][3] - self.current_y_val,
                            np.sum((action / weight_sum) * self.current_input_forecasts))
                else:
                    self.current_prediction = np.sum((action / weight_sum) * self.current_input_forecasts)"""
        elif self.mode == 'discrete':
            if action > 0:
                selection: np.ndarray = self._action_to_forecaster_selection(action)
                weight_sum: int = (selection == 1).sum()

                self.current_prediction = np.sum((action / weight_sum) * self.current_input_forecasts)
                """
                if self.config.extended_state:
                    if self.delay == 0:
                        self.current_prediction = (
                            np.sum((action / weight_sum) * self.current_input_forecasts),
                            self.current_y_val,
                            np.sum((action / weight_sum) * self.current_input_forecasts) - self.current_y_val,
                            np.sum((action / weight_sum) * self.current_input_forecasts))
                    else:
                        self.current_prediction = (
                            self.current_state[self.config.window_size - self.delay][3],
                            self.current_y_val,
                            self.current_state[self.config.window_size - self.delay][3] - self.current_y_val,
                            np.sum((action / weight_sum) * self.current_input_forecasts))
                else:
                    self.current_prediction = np.sum((action / weight_sum) * self.current_input_forecasts) """

    def _calculate_reward(self) -> float:
        """
        Calculates reward for current state followed by currently chosen action. The reward is
        computed accordingly to several recent predictions and the current prediction. The ensembler
        is treated as a forecaster, so that it could be compared with them by accuracy, and
        sorted in the priority queue. The lower rank in the queue, the better accuracy.

        Returns
        -------
        reward
            Numerical reward value; the "inverse" of the ensembler's rank in the sorted list of
            forecasters. Note that, the lower rank is followed by the higher reward i.e. the
            better accuracy, the better reward. The reward contains a
        """
        # The accuracy ranking of the most recent predictions. Firstly, the list is filled
        # with [forecaster_index, prediction].
        prediction_ranking: list = list(enumerate(self.current_input_forecasts))

        # Now, the ensembler is treated as additional forecaster. Assume that our ensembler
        # has the greatest index among forecasters.
        ensembler_index = self.config.input_size + (2 if self.mock_additional_forecasters else 0)
        prediction_ranking.append((ensembler_index, self.current_prediction))

        # Calculates the prediction errors.
        prediction_ranking = [(idx, abs(pred - self.current_y_val_true)) for (idx, pred) in prediction_ranking]

        self.prev_error = prediction_ranking[-1][1]  # An update for logging purposes
        prediction_ranking.sort(key=lambda x: x[1])  # Sorts the list to create the ranking.
        ensembler_rank: int = [x[0] for x in prediction_ranking].index(ensembler_index)

        # Difference between the best forecaster and ensembler's prediction makes a contribution
        # to the overall reward. The lesser difference, the greater reward. prediction_ranking is a list of
        # tuples; The first element of tuple is forecaster number and the second is the prediction.
        diff = prediction_ranking[ensembler_rank][1] - prediction_ranking[0][1]
        diff_factor = - diff * self.config.reward_diff_factor

        reward = (self.config.input_size + (2 if self.mock_additional_forecasters else 0)) - ensembler_rank
        reward += diff_factor
        return reward

    def _calculate_errors(self):
        self.current_error = self.current_prediction - self.current_y_val_true
        self.current_forecasters_errors = self.current_input_forecasts - self.current_y_val_true

    def add_next_true_value(self):
        if self.config.extended_state:
            self.delay -= 1
            self.current_y_val = self.data_y[self.current_index - self.delay]

            for i in range(self.config.window_size - 1):
                self.current_state[i][0] = self.current_state[i + 1][0]
                self.current_state[i][1] = self.current_state[i + 1][1]
                self.current_state[i][2] = self.current_state[i + 1][2]

            last_index = self.config.window_size - 1
            self.current_state[last_index][0] = self.current_state[last_index - self.delay][3]
            self.current_state[last_index][1] = self.current_y_val
            self.current_state[last_index][2] = self.current_state[last_index][0] - self.current_state[last_index][1]

        return self.current_state

    def step(self, action) -> Tuple[np.ndarray, float, bool, object]:
        """
        Computes the next state and reward for current state and given action. Updates class attributes
        i.e. moves to the next training example, updates current_state and current_reward. If the step
        was the last one during current episode, resets environment.

        Parameters
        ----------
        action : np.ndarray
            An array of weights corresponding to forecasters.

        Returns
        -------
        current_state : np.ndarray
            A state obtained by executing the action on the state, which was current before taking step.
            A class attribute current_state is now updated to the value, which is returned here.
        current_reward : float
            A reward obtained by the action on the state, which was current before taking step.
        done : bool
            Whether this was the last step of the episode.
        object: object
            Empty object returned to satisfy super method requirements.
        """
        self._take_action(action)
        self._calculate_errors()
        self.current_reward = self._calculate_reward()
        self._proceed_to_next_state()
        self.current_episode_len += 1
        done = self.current_episode_len == self.config.episode_length  # Reset after EPISODE_LENGTH steps
        if not done:
            # If the episode is over, the current step data will be loaded in reset function
            # anyway. This if prevents this from being done twice.
            self._load_current_step_data()
        self.add_next_true_value()
        #print(self.delay, self.current_state)
        return self.current_state, self.current_reward, done, {}

    def reset(self) -> np.ndarray:
        """
        Resets the environment in order to prepare it for the next episode. Populates the past
        values window (state) with random values, since no values have been predicted in the following
        episode yet.

        Returns
        -------
        current_state : np.ndarray
            Sampled initial state of the new episode. It cannot be computed, because no predictions
            has been done yet in the episode, hence it's sampled.
        """
        self.current_base_step = (self.current_base_step + self.config.episode_length) % self.total_step_num
        self.current_episode_len = 0
        self._load_current_step_data()
        self.current_state = self.observation_space.sample()  # Populates the past values window
        if self.config.extended_state:
            if self.config.window_size > self.delay:
                for i in range(self.config.window_size - self.delay):
                    self.current_state[i + self.delay][0] = self.current_state[i][3]
            for i in range(self.config.window_size):
                self.current_state[i][2] = self.current_state[i][0] - self.current_state[i][1]

        return self.current_state

    def render(self, mode='human', close=False):
        """
        Overrides function that render result of the current step.

        Parameters
        ----------
        mode : str
            Describes how to return and present computation data. Default value is set to 'human' which
            forces the environment to render to the current display or terminal and return nothing.
        close : bool
            todo description
        """
        pass

    def scroll_to_beginning(self):
        """
        Sets the 0-th timestamp as the current step ("resets time")
        """
        self.current_base_step = 0
        self.current_episode_len = 0
