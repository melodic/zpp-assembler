import os

import pandas as pd
import numpy as np
import neptune.new as neptune

from stable_baselines3 import SAC
from src.env.EnsemblerEnv import EnsemblerEnv
from src.env.EnsemblerEnvConfig import EnsemblerEnvConfig
from src.logging.NeptuneCallback import NeptuneCallback
from src.experiments.run import run_agent
from definitions import EXTERNAL_DATA_DIR, NEPTUNE_KEY_DIR
from utils.parse_yaml import parse
from utils.parse_config import parse_sac_config


def experiment_sac(config_file):
    experiment_config: dict = parse_sac_config(config_file)
    data: np.ndarray = (pd.read_csv(os.path.join(EXTERNAL_DATA_DIR, 'predictions.csv'))).to_numpy()

    training_example_num = np.count_nonzero(data[:, experiment_config['split_column']] == 'not_test')
    test_example_num = len(data) - training_example_num
    training_data = data[:training_example_num]
    test_data = data[training_example_num:]

    run = neptune.init(
        api_token=parse(NEPTUNE_KEY_DIR)['API_KEY'],
        project=experiment_config['project_name'],
        name=experiment_config['experiment_name'],
    )

    del experiment_config['split_column']
    del experiment_config['project_name']

    run["parameters"] = experiment_config
    env_config = EnsemblerEnvConfig(reward_diff_factor=experiment_config['reward_diff_factor'],
                                    episode_length=experiment_config['episode_length'],
                                    extend_predictions_by=experiment_config['extend_predictions_by'],
                                    extended_state=experiment_config['extended_state'])
    train_env = EnsemblerEnv(data=training_data, config=env_config)
    test_env = EnsemblerEnv(data=test_data, config=env_config)

    model = SAC('MlpPolicy',
                train_env,
                verbose=1,
                batch_size=experiment_config['batch_size'],
                gamma=experiment_config['gamma'],
                learning_rate=experiment_config['learning_rate'])

    run_agent(model, test_env, run, test_example_num, trained=False)
    model.learn(total_timesteps=experiment_config['data_traversals'] * training_example_num,
                callback=NeptuneCallback(experiment_config['episode_length'], train_env, run))
    test_env.scroll_to_beginning()
    run_agent(model, test_env, run, test_example_num, trained=True)

    run.stop()
