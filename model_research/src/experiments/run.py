import neptune.new as neptune
import numpy as np

from src.env.EnsemblerEnv import EnsemblerEnv
from utils.loss import MSE, MSLE

LOG_INTERVAL = 10
RUN_NUM = 5


def run_agent(model: object, env: EnsemblerEnv, run: neptune.Run, example_num: int, trained: bool, forecasters: int = 5):
    """
    Runs an instance of learning agent on a dataset RUN_NUM times and logs outcomes.

    Parameters
    ----------
    model : object
        The model used to be used in the run. It's an instance of PPO, DQN or SAC classes
        from stable_baseline3 library. It is already provided with training/validation data.
    env : EnsemblerEnv
        Environment used for training.
    run : neptune.Run
        Initialized Neptune run object, for logging purposes.
    example_num : int
        Number of training/validation examples.
    trained : bool
        Whether a trained or untrained agent is performing the run. It determines the logging location.
    forecasters: todo
    """
    error_sum = 0
    model_results = np.zeros(RUN_NUM * example_num)
    forecasters_results = np.zeros(shape=(forecasters, RUN_NUM * example_num))
    grand_truths = np.tile(env.data_y, RUN_NUM)

    obs = env.reset()
    for j in range(RUN_NUM):
        interval_error_sum = 0
        for i in range(example_num):
            action, _states = model.predict(obs)
            obs, rewards, done, info = env.step(action)
            error_sum += env.prev_error
            interval_error_sum += env.prev_error
            current_best_forecast = np.min(np.abs(env.current_forecasters_errors))

            model_results[j * example_num + i] = env.current_prediction
            for x in range(forecasters):
                forecasters_results[x][j * example_num + i] = env.current_input_forecasts[x]

            scope = "trained" if trained else "random"
            run["step_error/" + scope].log(env.prev_error)
            run["step_error/best-forecaster"].log(current_best_forecast)

            if env.current_episode_len % LOG_INTERVAL == 0:
                interval_mean_error = interval_error_sum / LOG_INTERVAL
                if trained:
                    run["interval_error/trained"].log(interval_mean_error)
                else:
                    run["interval_error/random"].log(interval_mean_error)
                interval_error_sum = 0
            if done:
                env.reset()

        env.scroll_to_beginning()

    scope = "trained" if trained else "random"
    run["experiment_error/" + scope] = error_sum / (RUN_NUM * example_num)
    run["MSE/" + scope + "/model"] = MSE(model_results, grand_truths)
    run["MSLE/" + scope + "/model"] = MSLE(model_results, grand_truths)
    for i in range(forecasters):
        run["MSE/" + scope + "/forecaster-" + str(i)] = MSE(forecasters_results[i], grand_truths)
        run["MSLE/" + scope + "/forecaster-" + str(i)] = MSLE(forecasters_results[i], grand_truths)
