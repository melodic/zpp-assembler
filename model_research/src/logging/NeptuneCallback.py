import numpy as np
import neptune.new as neptune

from src.env.EnsemblerEnv import EnsemblerEnv
from stable_baselines3.common.callbacks import BaseCallback


class NeptuneCallback(BaseCallback):
    """
    Custom callback for plotting rewards in Neptune.

    Attributes
    ----------
    episode_len : int
        The number of steps during each episode.
    env : EnsemblerEnv
        Environment used during training
    run : neptune.Run
        todo descr
    verbose : int
        todo descr
    current_episode_reward_sum : float
        Summed up rewards obtained so far in the episode.
    current_epoch_reward_sum : float
        Summed up rewards obtained so far in the epoch.
    """

    def __init__(self, episode_len: int, env: EnsemblerEnv, run: neptune.Run, verbose=0):
        super(NeptuneCallback, self).__init__(verbose)
        self.episode_len: int = episode_len
        self.env: EnsemblerEnv = env
        self.run: neptune.Run = run
        self.current_episode_reward_sum: float = 0
        self.current_epoch_reward_sum: float = 0
        self.current_error: float = 0

    def _on_step(self) -> bool:
        """
        Callback called after each training step.

        Returns
        -------
        True : bool
            Always return true boolean value to satisfy super method requirements.
        """
        self.current_episode_reward_sum += self.env.current_reward
        self.current_epoch_reward_sum += self.env.current_reward
        self.current_error = np.abs(self.env.current_error)
        self.current_best_forecaster_error = np.min(np.abs(self.env.current_forecasters_errors))
        self.current_worst_forecaster_error = np.max(np.abs(self.env.current_forecasters_errors))

        self.run["ensembler/training/step/error"].log(abs(self.current_error))
        self.run[f"training/step/error/diff/best"].log(np.abs(self.current_best_forecaster_error - self.current_error))
        self.run[f"training/step/error/diff/worst"].log(np.abs(self.current_worst_forecaster_error - self.current_error))

        if self.env.current_episode_len == self.episode_len - 1:
            self.run["ensembler/training/episode/sum_of_rewards"].log(self.current_episode_reward_sum)
            self.current_episode_reward_sum = 0

            if self.env.current_base_step == self.env.total_step_num - self.episode_len:
                self.run["ensembler/training/epoch/sum_of_rewards"].log(self.current_epoch_reward_sum)
                self.current_epoch_reward_sum = 0

        return True
