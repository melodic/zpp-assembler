import os


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = os.path.join(ROOT_DIR, 'data')
EXTERNAL_DATA_DIR = os.path.join(DATA_DIR, 'external')
TMP_DIR = os.path.join(ROOT_DIR, 'tmp')
DQN_TMP_DIR = os.path.join(TMP_DIR, 'dqn')
PPO_TMP_DIR = os.path.join(TMP_DIR, 'ppo')
NEPTUNE_KEY_DIR = os.path.join(ROOT_DIR, 'neptune_key.yaml')
EXPERIMENTS_DIR = os.path.join(ROOT_DIR, 'src/experiments')
PPO_EXPERIMENT_CONFIGS_DIR = os.path.join(EXPERIMENTS_DIR, 'ppo/configs')
DQN_EXPERIMENT_CONFIGS_DIR = os.path.join(EXPERIMENTS_DIR, 'dqn/configs')
A2C_EXPERIMENT_CONFIGS_DIR = os.path.join(EXPERIMENTS_DIR, 'a2c/configs')
DDPG_EXPERIMENT_CONFIGS_DIR = os.path.join(EXPERIMENTS_DIR, 'ddpg/configs')
SAC_EXPERIMENT_CONFIGS_DIR = os.path.join(EXPERIMENTS_DIR, 'sac/configs')
ML_MODELS_DIR = os.path.join(DATA_DIR, 'models')

