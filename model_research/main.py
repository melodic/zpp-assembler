from src.experiments.dqn.experiment_dqn import experiment_dqn
from src.experiments.ppo.experiment_ppo import experiment_ppo
from src.experiments.ddpg.experiment_ddpg import experiment_ddpg
from src.experiments.sac.experiment_sac import experiment_sac
from src.experiments.a2c.experiment_a2c import experiment_a2c

if __name__ == '__main__':
    # experiment_sac('default.yaml')
    # experiment_sac('default_extended.yaml')
    # experiment_sac('all_features_old_state.yaml')
    # experiment_sac('all_features.yaml')
    # experiment_sac('continuous_reward.yaml')
    # experiment_sac('continuous_reward_extended_state.yaml')
    # experiment_sac('mocked_forecasters.yaml')
    # experiment_sac('mocked_forecasters_extended_state.yaml')

    # experiment_dqn('default.yaml')
    # experiment_dqn('default_extended.yaml')
    # experiment_dqn('all_features_old_state.yaml')
    # experiment_dqn('all_features.yaml')
    # experiment_dqn('continuous_reward.yaml')
    # experiment_dqn('continuous_reward_extended_state.yaml')
    # experiment_dqn('mocked_forecasters.yaml')
    # experiment_dqn('mocked_forecasters_extended_state.yaml')

    experiment_ppo('default.yaml')
    experiment_ppo('default_extended.yaml')
    experiment_ppo('all_features_old_state.yaml')
    experiment_ppo('all_features.yaml')
    experiment_ppo('continuous_reward.yaml')
    experiment_ppo('continuous_reward_extended_state.yaml')
    experiment_ppo('mocked_forecasters.yaml')
    experiment_ppo('mocked_forecasters_extended_state.yaml')

    """
    experiment_a2c('default.yaml')
    experiment_a2c('default_extended.yaml')
    experiment_a2c('all_features_old_state.yaml')
    experiment_a2c('all_features.yaml')
    experiment_a2c('continuous_reward.yaml')
    experiment_a2c('continuous_reward_extended_state.yaml')
    experiment_a2c('mocked_forecasters.yaml')
    experiment_a2c('mocked_forecasters_extended_state.yaml')
    """

    # experiment_ddpg('default.yaml')
    # experiment_ddpg('default_extended.yaml')
    # experiment_ddpg('all_features_old_state.yaml')
    # experiment_ddpg('all_features.yaml')
    # experiment_ddpg('continuous_reward.yaml')
    # experiment_ddpg('continuous_reward_extended_state.yaml')
    # experiment_ddpg('mocked_forecasters.yaml')
    # experiment_ddpg('mocked_forecasters_extended_state.yaml')
