# ZPP Ensembler

*A dynamic ensembling component for time series forecasting, based on reinforcement learning*

### Project structure
- `integration` dir: interactive ensembling component + testing environment
- `model_research` dir: model development experiments (equivalent to the [main](https://gitlab.ow2.org/melodic/zpp-assembler/-/tree/main) branch)

### Authors (in alphabetical order)
- [Mikołaj Drzewiecki](mailto:mikolajdrzewiecki.1999@gmail.com)
- [Stanisław Durka](mailto:stas.durka@gmail.com)
- [Sebastian Miller](mailto:smiller@7bulls.com)
- [Franciszek roszkiewicz](mailto:froszkiewicz@gmail.com)

### Thesis
A [bachelor's thesis](https://apd.uw.edu.pl/diplomas/214222/) for the [Faculty of Mathematics, Informatics and Mechanics of the University of Warsaw (MIMUW)](https://www.mimuw.edu.pl/en) has been produced on the basis of this project. Its full text is available in the [rl-ensembling-thesis.pdf](rl-ensembling-thesis.pdf) file.
