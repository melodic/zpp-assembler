from stable_baselines3 import PPO
import random
import logging
from gym import spaces

from ensembler_app.directories import PROPERTIES_PATH
from ensembler_app.utils import parse

conf = parse(PROPERTIES_PATH)

common_observation_space: spaces.Box = spaces.Box(
    low=0.0, high=conf['training-data-params']['max_prediction_value'], shape=(conf['hyperparameters']['window_size'],)
)

EPISODE_LENGTH = conf['hyperparameters']['episode_length']
MAX_PREDICTION_VALUE = conf['training-data-params']['max_prediction_value']

class ExtendedModel:
    def __init__(
        self,
        model: PPO,
        input_forecasters: list,
        forecaster_positions: dict,
        publish_rate: int
    ):
        self.model = model
        self.input_forecasters: list = input_forecasters
        self.forecaster_positions: dict = forecaster_positions
        self.current_state = common_observation_space.sample()
        self.cur_episode_len = 0
        self.previous_prediction = random.random() * MAX_PREDICTION_VALUE #TODO trzeba zupełnie zmienić to max_prediction_value bo przecież nie znamy tego
        self.publish_rate = publish_rate

    def update_state(self):
        """
        Updates the state, that is:
            - pops the oldest prediction from the window
            - appends the most recent prediction to the window
        """
        logging.info("Updating state.")

        self.cur_episode_len += 1

        if self.cur_episode_len == EPISODE_LENGTH:
            # Reset state after each episode.
            self.cur_episode_len = 0
            self.current_state = common_observation_space.sample()
            self.previous_prediction = random.random() * MAX_PREDICTION_VALUE
        else:
            self.current_state[:-1] = self.current_state[1:]   # pops
            self.current_state[-1] = self.previous_prediction  # appends

        logging.info(f"New state: {self.current_state}")

    def predict(self):
        logging.info(f"Producing weights.")
        return self.model.predict(self.current_state)[0]
