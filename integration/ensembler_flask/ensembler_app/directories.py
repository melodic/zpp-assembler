import os

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SAVED_MODELS_DIR = os.path.join(ROOT_DIR, 'saved_models')
LOGS_DIR = os.path.join(ROOT_DIR, 'logs')

CONFIG_DIR = os.path.join(ROOT_DIR, 'config')
PROPERTIES_PATH = os.path.join(CONFIG_DIR, 'properties.yml')
