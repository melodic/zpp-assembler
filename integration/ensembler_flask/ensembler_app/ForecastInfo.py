class ForecastInfo:
    """
    A helper class responsible for storing the JSON
    data received from the PO.

    Parameters
    ----------
    info_json
        Data from PO
    """
    def __init__(self, info_json):
        self.metric: str = info_json["metric"]
        self.prediction_time: int = info_json["predictionTime"]
        self.predictions: dict = info_json["predictionsToEnsemble"]