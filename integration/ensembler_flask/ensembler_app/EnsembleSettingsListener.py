import stomp
import json
import logging
from ensembler_app.Predictor import Predictor

class EnsembleSettingsListener(stomp.ConnectionListener):
    """
    A class that specializes the ConnectionListener class from
    the stomp.py library.

    The listener is responsible for gathering the "start_ensembler"
    messages from the MQ and initiating model training by calling
    an appropriate Predictor method.

    Parameters
    ----------
    predictor: Predictor
        The globally used predictor
    """
    def __init__(self, predictor: Predictor):
        self.predictor = predictor

    def on_message(self, frame):
        """
        Initiates model training after receiving the 'start_ensembler'
        message from the queue.

        Parameters
        ----------
        frame
            The 'start_ensembler' message from the queue
        """
        logging.info("Received start_ensembler message from ActiveMQ.")

        start_ensembler = json.loads(frame.body)
        logging.debug("start_ensembler:")
        logging.debug(start_ensembler)

        self.predictor.train_new_models(start_ensembler)
