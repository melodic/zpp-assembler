import yaml

def parse(filename: str) -> dict:
    with open(filename, "r") as stream:
        return yaml.safe_load(stream)
