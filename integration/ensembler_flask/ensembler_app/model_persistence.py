"""
Utility functions for saving and loading the ExtendedModel
class instances (including the proper persistence of the
inner Stable-Baselines3 model object).
"""

from ensembler_app.ExtendedModel import ExtendedModel
from ensembler_app.directories import SAVED_MODELS_DIR

from stable_baselines3 import PPO
from typing import Tuple

import os
import json
import copy
import logging

def save_model(metric: str, model: PPO):
    """
    Saves the given Stable-Baselines3 model object to a properly named zip file.

    Parameters
    ----------
    metric: str
        The name of the metric whose predictions the model ensembles
    model: PPO
        The Stable-Baselines3 model object to be saved
    """
    logging.info(f"Persisting the Stable-Baselines3 model object for: {metric}.")

    model_zip_file_path = os.path.join(SAVED_MODELS_DIR, metric, f"{metric}.zip")
    model.save(model_zip_file_path)

    logging.info(f"Successfully persisted the Stable-Baselines3 model object for: {metric}.")

def load_model(metric: str) -> PPO:
    """
    Loads the persisted Stable-Baselines3 model object associated with the given metric.

    Parameters
    ----------
    metric: str
        The name of the metric whose predictions the model to be loaded ensembles
    """
    logging.info(f"Loading the Stable-Baselines3 model object for: {metric}.")

    model_zip_file_path = os.path.join(SAVED_MODELS_DIR, metric, f"{metric}.zip")
    if not os.path.exists(model_zip_file_path):
        logging.error(f"Cannot load the model for {metric}. The model object zip does not exist.")
        return None

    logging.info(f"Successfully loaded the Stable-Baselines3 model object for: {metric}.")

    return PPO.load(model_zip_file_path)

def generate_paths(metric: str) -> Tuple[str, str]:
    """
    Utility function that generates path strings required in saving/loading.

    Parameters
    ----------
    metric: str
        The name of the metric with which the saved/loaded model is associated

    Returns
    -------
    A tuple with the paths: (path to the saved models directory,
                             path to the saved model json file)
    """
    model_dir_path = os.path.join(SAVED_MODELS_DIR, metric)
    model_json_path = os.path.join(model_dir_path, f"{metric}.json")

    return model_dir_path, model_json_path

def save_ext_model(metric: str, ext_model: ExtendedModel):
    """
    Persists the entire given ExtendedModel object, including its internal objects
    in a proper directory.

    Parameters
    ----------
    metric: str
        The name of the metric whose predictions the model ensembles
    ext_model: ExtendedModel
        The ExtendedModel object to be saved
    """
    logging.info(f"Persisting the ExtendedModel object for: {metric}.")

    model_dir_path, model_json_path = generate_paths(metric)

    # Separate directory for each metric's model.
    if not os.path.exists(model_dir_path):
        os.mkdir(model_dir_path)

    # Save the internal Stable-Baselines3 model object.
    save_model(metric, ext_model.model)

    # We do not want to delete the original object's fields,
    # even for a moment.
    ext_model_copy = copy.copy(ext_model)

    # Skip the non-serializable properties, as well as those
    # unnecessary for object init.
    del ext_model_copy.model
    del ext_model_copy.current_state
    del ext_model_copy.cur_episode_len
    del ext_model_copy.previous_prediction

    # Save the remaining fields to JSON.
    with open(model_json_path, "w") as model_json:
        json.dump(ext_model_copy, model_json, default=lambda m: m.__dict__)
        logging.info(f"Persisted the remaining ExtendedModel object fields for: {metric}.")

    logging.info(f"Persisted the ExtendedModel object for: {metric}.")

def json_to_ext_model(model, model_json) -> ExtendedModel:
    """
    Converts the serialized ExtendedModel in JSON form
    to an actual ExtendedModel object.

    Parameters
    ----------
    model
        The Stable-Baselines3 model object
    model_json
        The model in json form

    Returns
    -------
    The decoded ExtendedModel object (missing model and current_state attributes
    since they are always null in the saved JSON).
    """
    return ExtendedModel(
        model,
        model_json["input_forecasters"],
        model_json["forecaster_positions"],
        model_json["publish_rate"]
    )

def load_ext_model(metric: str) -> ExtendedModel:
    """
    Loads the entire persisted ExtendedModel object associated with the given metric.

    Parameters
    ----------
    metric: str
        The name of the metric with which the loaded model is associated

    Returns
    -------
    The loaded ExtendedModel object or None if no such model is saved
    """
    logging.info(f"Loading the ExtendedModel object for: {metric} from persisetence.")

    model_dir_path, model_json_path = generate_paths(metric)

    if not os.path.exists(model_dir_path):
        logging.error(f"Cannot load model for {metric}. Its directory does not exist.")
        return None

    if not os.path.exists(model_json_path):
        logging.error(f"Cannot load model for {metric}. Its JSON dump does not exist.")
        return None

    model = load_model(metric)
    if model is None:
        return None

    with open(model_json_path, "r") as model_json:
        ext_model_json = json.load(model_json)
        logging.info(f"Loaded the remaining ExtendedModel object fields for: {metric}.")

    return json_to_ext_model(model, ext_model_json)

def load_all_ext_models() -> dict:
    """
    Loads all persisted models and returns them as dict mapping from
    metric name to the proper ExtendedModel object.

    Returns
    -------
    A dict: { <metric_name>: <ExtendedModel object> }
    """
    logging.info("Loading all persited models from disk.")

    models = {}

    for metric_dir in os.scandir(SAVED_MODELS_DIR):
        if os.path.isdir(metric_dir):
            loaded_ext_model = load_ext_model(metric_dir.name)
            if load_ext_model is not None:
                logging.info(f"Successfully loaded persisted model for: {metric_dir.name}")
                models[metric_dir.name] = loaded_ext_model

    logging.info("Successfully loaded all persisted models.")

    return models