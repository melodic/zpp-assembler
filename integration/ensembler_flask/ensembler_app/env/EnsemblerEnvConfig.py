from ensembler_app.directories import PROPERTIES_PATH
from ensembler_app.utils import parse

conf = parse(PROPERTIES_PATH)
hyperparameters = conf['hyperparameters']
training_data_params = conf['training-data-params']

class EnsemblerEnvConfig:
    """
    A class containing information about training data structure used by EnsemblerEnv.

    Attributes
    ----------
        input_size : int
            The number of input forecasters.
        window_size : int
            The number of past predictions that are used as the state.
        episode_length : int
            The number of steps in each episode.
        max_prediction_value : float
            Upper bound of prediction value. Used by the environment for creating sample initial
            state at the beginning of each episode.
        first_prediction_column : int
            The index of the first column of data that stores predictions for every time step.
        grand_truth_column : int
            The index of the column of data that stores grand truth for every time step.
        mode : str
            Defines action space. One of the following: 'discrete' or 'continuous'. 'discrete' mode
            implies action space to be an instance of Discrete class. 'continuous' mode implies
            action space to be an instance of Box class.
        extend_predictions_by : float
            Defines the gap between values of additional (mocked) forecasters and the bounds of
            initial set of predictions. Setting the value to 0, disables creating additional
            mocked forecasters.
        reward_diff_factor : float
            The contribution of the difference between the best forecaster's prediction and ensembler's
            prediction to the reward.
        extended_state : bool
            Is environment using extended state.
    """
    def __init__(
        self,
        input_size: int,
        window_size: int = hyperparameters['window_size'],
        episode_length: int = hyperparameters['episode_length'],
        max_prediction_value: float = training_data_params['max_prediction_value'],
        first_prediction_column: int = training_data_params['first_prediction_column'],
        grand_truth_column: int = training_data_params['grand_truth_column'],
        mode: str = hyperparameters['mode'],
        extend_predictions_by = hyperparameters['extend_predictions_by'],
        reward_diff_factor = hyperparameters['reward_diff_factor'],
        extended_state = hyperparameters['extended_state']
    ):
        self.input_size = input_size
        self.window_size = window_size
        self.episode_length = episode_length
        self.max_prediction_value = max_prediction_value
        self.first_prediction_column = first_prediction_column
        self.grand_truth_column = grand_truth_column
        self.mode = mode
        self.extend_predictions_by: float = extend_predictions_by
        self.reward_diff_factor: float = reward_diff_factor
        self.extended_state: bool = extended_state
