from influxdb import InfluxDBClient

import pandas as pd
import numpy as np

from time import sleep

from typing import Tuple, Optional

from stable_baselines3 import PPO

import logging
import os

from ensembler_app.ForecastInfo import ForecastInfo
from ensembler_app.env.EnsemblerEnvConfig import EnsemblerEnvConfig
from ensembler_app.env.EnsemblerEnv import EnsemblerEnv
from ensembler_app.ExtendedModel import ExtendedModel
from ensembler_app.model_persistence import load_all_ext_models, save_ext_model, load_ext_model, save_model
from ensembler_app.directories import PROPERTIES_PATH
from ensembler_app.utils import parse

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.executors.pool import ThreadPoolExecutor

conf = parse(PROPERTIES_PATH)
hyperparameters = conf['hyperparameters']
training_data_params = conf['training-data-params']

UNIT_TESTS_MODE = os.environ.get('UNIT_TESTS_MODE', 'False') == 'True'

NS_IN_MS = 10**6
MS_IN_S = 10**3

class Predictor:
    """
    A class responsible for producing ensembled predictions,
    based on forecasts received from the Prediction Orchestrator's
    requests, using appropriately trained RL models.
    """
    def __init__(self, influxdb_client: InfluxDBClient):
        if not UNIT_TESTS_MODE:
            if influxdb_client is None:
                raise RuntimeError('Unexpected error: influxdb_client arg is None, even though unit tests mode is off.')

            self.influxdb_client = influxdb_client

            self.models: dict[str, ExtendedModel] = load_all_ext_models()

            # Scheduler for planning the recurrent continual model training.
            logging.info("Initializing the scheduler.")
            self.scheduler = BackgroundScheduler(
                executors = { "default": ThreadPoolExecutor(conf['misc']['training_thread_pool_size']) }
            )
            logging.info("Successfully initialized the scheduler.")

            logging.info("Initializing continual training scheduler jobs.")
            for metric_name, model in self.models.items():
                self._add_continual_training_job(metric_name, model.publish_rate)
            logging.info("Successfully initialized all continual training scheduler jobs.")

            logging.info("Starting the scheduler.")
            self.scheduler.start()
            logging.info("Successfully started the scheduler.")

    def _add_continual_training_job(self, metric_name, publish_rate):
        continual_training_interval = self._calculate_model_update_interval(publish_rate)

        self.scheduler.add_job(
            func=self.update_model,
            trigger='interval',
            seconds=continual_training_interval,
            args=[metric_name],
            id=metric_name,
            misfire_grace_time=None,
            replace_existing=True
        )
        logging.info(f"Scheduled the continual training job for: {metric_name} to occur every: {continual_training_interval}s.")

    def _remove_continual_training_job(self, metric_name: str):
        """
        Removes the continual training job from the scheduler associasted
        with the given metric, provided it exists.

        Parameters
        ----------
        metric_name: str
            The name of the metric with which the removed job is associated
        """
        if self.scheduler.get_job(metric_name, jobstore='default') is not None:
            self.scheduler.remove_job(metric_name, jobstore='default')
            logging.debug(f"Removed old job for {metric_name} continual training.")

    def _prepare_forecaster_positions(self, forecaster_models: list) -> dict:
        """
        Since the models require the input forecasts to be ordered the same way
        every time, and forecasts received from the PO are unordered, a mapping of
        forecaster names to proper positions is needed so that the ordering can
        be reproduced with every request.

        This utility method produces such mapping for a given list of forecaster names.

        Parameters
        ----------
        forecaster_models: list
            The names for whom the mapping is produced

        Returns
        -------
        forecaster_positions: dict
            A mapping ("forecaster name") -> ("the position at which the model
                                               expects the forecaster's prediction to be")
        """
        return dict(zip(forecaster_models, range(len(forecaster_models))))

    def _prepare_forecaster_field_names(self, forecaster_models: list) -> str:
        """
        Produces a string used in the InfluxQL SELECT query with field names
        adequate to the supplied forecaster names.

        Parameters
        ----------
        forecaster_models: list
            Ordered list of input forecaster model names

        Returns
        -------
        A string to be used in an appropriate InfluxQL query
        """
        return ','.join([f'{model}_value' for model in forecaster_models])

    def _trim_data(self, data: pd.DataFrame):
        """
        Deletes the initial rows from the DataFrame which contain
        only NaNs in all of the prediction columns.
        Since we merge with the real values treated as the 'left'
        column, no NaNs will be in the real value column in the merged
        dataframe.

        Parameters
        ----------
        data: pd.DataFrame
            The real values and prediction dataframes merged into one

        Returns
        -------
        The trimmed DataFrame
        """

        # There might be some dangling real values before the first prediction.
        first_valid_prediction_index = data.iloc[:, training_data_params['first_prediction_column']:].first_valid_index()
        
        # If all rows have all NaN columns, then we need to trim all rows.
        if first_valid_prediction_index is None:
            return data.head(0)

        return data.truncate(before=first_valid_prediction_index)

    def _split_data_into_cohesive_runs(self, data: pd.DataFrame, publish_rate: int):
        """
        Splits the data downloaded from the database into segments which
        are expected to have come from cohesive system runs and not
        separate runs.

        Parameters
        ----------
        data: pd.DataFrame
            The real values and prediction dataframes merged into one
        publish_rate: int
            The publish rate of the metric whose values are in data

        Returns
        -------
        A list of DataFrames of data from cohesive runs
        """
        ts_col = data.columns.get_loc("time")

        # Obtain timestamp differences between any two consecutive rows.
        ts_diffs = np.array([
            abs(int(data.iloc[i, ts_col]) - int(data.iloc[i + 1, ts_col]))
            for i in range(data.shape[0] - 1)
        ])

        # The minimum difference between two consecutive timestamps that
        # entails two separate runs.
        splitting_diff = training_data_params['run_break_len_factor'] * publish_rate * NS_IN_MS

        # Obtain the indices of the last rows from each cohesive run.
        last_from_run_indices = (ts_diffs > splitting_diff).nonzero()[0].tolist()

        # Add the last index from the latest cohesive run.
        last_from_run_indices.append(data.shape[0] - 1)

        cohesive_runs_data = []
        cur_run_first_idx = 0

        # Split the data into cohesive segments.
        for i in last_from_run_indices:
            next_run_first_idx = i + 1
            cohesive_runs_data.append(
                data.iloc[cur_run_first_idx : next_run_first_idx, :]
            )
            cur_run_first_idx = next_run_first_idx

        return cohesive_runs_data

    def _trim_all_runs_data(self, runs_data: list):
        """
        Applies the _trim_data method to all DataFrames in the given list.

        Parameters
        ----------
        runs_data: list
            A list of DataFrames representing data from cohesive runs.
        """
        for i in range(len(runs_data)):
            runs_data[i] = self._trim_data(runs_data[i])

    def _eliminate_too_short_runs_data(self, runs_data: "list[pd.DataFrame]"):
        """
        Removes those DataFrames from the runs_data list which contain
        too little data to perform training on them.

        Parameters
        ----------
        runs_data: list
            A list of DataFrames representing data from cohesive runs.

        Returns
        -------
        runs_data without runs that are too short
        """
        return [
            df for df in runs_data
            if df.shape[0] * hyperparameters['dataset_traversals'] >= hyperparameters['n_steps']

            # There is no point in running training over data from some run if not
            # even a single policy update would be performed during this training
            # (a policy update is performed after n_steps steps).
        ]

    def _obtain_real_values_(self, metric: str, extended_set_size: int) -> pd.DataFrame:
        """
        Makes a query for appropriate real metric values to InfluxDB
        and returns a properly ordered DataFrame with the results.

        Parameters
        ----------
        metric: str
            The name of the metric whose real values are queried
        extended_set_size: int
            The max number of queried datapoints

        Returns
        -------
        A DataFrame with the real metric values
        """
        logging.debug("Obtaining real values from InfluxDB.")
        real_values_data_raw = self.influxdb_client.query(
            query='SELECT time,value '
                  f'FROM {metric} '
                  'ORDER BY time DESC '
                  f'LIMIT {extended_set_size};',
            database=training_data_params['real_values_db_name'],
            epoch='ns'
        ).get_points()

        # Dataframe in ascending timestamp order.
        real_values_data_raw = pd.DataFrame(real_values_data_raw)[::-1].reset_index(drop=True)
        logging.debug("Obtained real values from InfluxDB:")
        logging.debug(real_values_data_raw)

        return real_values_data_raw

    def _obtain_prediction_values(
            self,
            metric: str,
            extended_set_size: int,
            forecaster_models: list,
            latest_real_value_ts: int
        ) -> pd.DataFrame:
        """
        Makes a query for appropriate metric value predictions to InfluxDB
        and returns a properly ordered DataFrame with the results.

        Parameters
        ----------
        metric: str
            The name of the metric whose predictions are queried
        extended_set_size: int
            The max number of queried datapoints
        forecaster_models: list
            List of input forecaster names
        latest_real_value_ts: int
            The timestamp of the latest real metric value downloaded
            before - no point in downloading predictions for further
            timestamps.

        Returns
        -------
        A DataFrame with the metric value predictions
        """
        logging.debug("Obtaining prediction values from InfluxDB.")
        predictions_data_raw = self.influxdb_client.query(
            query=f'SELECT time,{self._prepare_forecaster_field_names(forecaster_models)} '
                  f'FROM {metric}Predictions '
                  f'WHERE time <= {latest_real_value_ts} '
                  'ORDER BY time DESC '
                  f'LIMIT {extended_set_size};',
            database=training_data_params['predictions_db_name'],
            epoch='ns'
        ).get_points()

        predictions_data_raw = pd.DataFrame(predictions_data_raw)[::-1].reset_index(drop=True)

        logging.debug("Obtained prediction values from InfluxDB:")
        logging.debug(predictions_data_raw)

        return predictions_data_raw

    def _merge_real_and_prediction_values(
            self,
            real_values_data_raw: pd.DataFrame,
            predictions_data_raw: pd.DataFrame,
            publish_rate: int
        ) -> pd.DataFrame:
        """
        Merges the downloaded DataFrames with historical real metric values
        and predictions based on their timestamps - in order to perform trianing,
        each real metric value needs to be matched with the predictions for the
        same (or similar) timestamp, so that accuracy can be judged.

        Parameters
        ----------
        real_values_data_raw: pd.DataFrame
            DataFrame with real metric values
        predictions_data_raw: pd.DataFrame
            DataFrame with predictions (matching the real metric values)
        publish_rate: int
            The publish rate of the considered metric
        """
        data = pd.merge_asof(real_values_data_raw, predictions_data_raw,
                             on='time', direction='nearest', tolerance=publish_rate * NS_IN_MS)
        logging.debug("Merged real values and predictions:")
        logging.debug(data)

        return data

    def _get_training_data(
            self,
            metric: str,
            forecaster_models: list,
            training_set_size: int,
            publish_rate: int
        ) -> Tuple[
                Optional[Tuple["list[np.ndarray]", int]],
                int
            ]:
        """
        Loads appropriate historical data (from the database) that will
        be used for training a new model.

        Parameters
        ----------
        metric: str
            The name of the metric whose values the new model should predict.
            The loaded historical data concerns this metric
        forecaster_models: list
            A list of the names of the forecasters on whose predictions the new ensembling
            model is to be based
        training_set_size: int
            The number of data points to be downloaded
        publish_rate: int
            The publish rate of the metric whose values are to be downloaded

        Returns
        -------
        (data, example_num), where:
        data: list[np.ndarray]
            The data that will be used for model training.
        example_num: int
            The total length of runs included in data.
        """
        logging.info("Obtaining training data from InfluxDB.")

        # Get a little more data than needed in order to reduce the likelihood
        # of losing too much data (and thus waiting) in the process of data pruning.
        extended_set_size = int((1 + training_data_params['training_set_size_eps']) * training_set_size)

        real_values_data_raw = self._obtain_real_values_(metric, extended_set_size)

        # If too little data has been obtained here, there is no point in its further processing.
        real_value_num = real_values_data_raw.shape[0]
        if real_value_num < training_set_size:
            return None, real_value_num

        latest_real_value_ts = int(real_values_data_raw.iloc[-1]["time"])

        logging.debug("Obtaining prediction values from InfluxDB.")
        predictions_data_raw = self._obtain_prediction_values(
            metric,
            extended_set_size,
            forecaster_models,
            latest_real_value_ts
        )

        # If too little data has been obtained here, there is no point in its further processing.
        predictions_num = predictions_data_raw.shape[0]
        if predictions_num == 0:
            return None, predictions_num

        data = self._merge_real_and_prediction_values(
            real_values_data_raw,
            predictions_data_raw,
            publish_rate
        )

        cohesive_runs_data = self._split_data_into_cohesive_runs(data, publish_rate)
        self._trim_all_runs_data(cohesive_runs_data)
        cohesive_runs_data = self._eliminate_too_short_runs_data(cohesive_runs_data)
        logging.debug("Pruned the obtained data:")
        logging.debug(cohesive_runs_data)

        run_lens = [df.shape[0] for df in cohesive_runs_data]
        total_example_num = sum(run_lens)
        if total_example_num < training_set_size:
            # We do not have enough data to perform fully valuable training.
            return None, total_example_num
    
        # We have enough data to perform training.
        return zip([df.to_numpy() for df in cohesive_runs_data], run_lens), total_example_num

    def _wait_for_enough_data(self, training_set_size: int, collected_data_size: int, publish_rate: int):
        """
        Sleeps for as long as it is needed for enough new data
        to perform training to appear in the database.

        Parameters
        ----------
        training_set_size: int
            Expected training set size
        collected_data_size: int
            How much training data has been collected
        publish_rate: int
            The publish rate of the collected data
        """
        sleep_time = (training_set_size - collected_data_size) * (publish_rate / MS_IN_S)
        logging.info(f"Will wait for {sleep_time} seconds.")
        sleep(
            sleep_time
            #TODO tu może jakiś epsilon dłużej żeby wziąć pod uwagę jakieś opóźnienia
        )

    def _prepare_envs(self, input_forecaster_num: int, training_data: list) -> list:
        """
        Creates a separate env for each cohesive run in the training data.

        Parameters
        ----------
        input_forecaster_num: int
            The number of input forecasters (on which the new model will be based)
        training_data: list
            A list of tuples (data_from_cohesive_run, cohesive_run_len)

        Returns
        -------
        A list of envs and lengths of the runs with which each env is associated
        """
        config = EnsemblerEnvConfig(input_size=input_forecaster_num)

        return [(EnsemblerEnv(run_data, config), run_len) for (run_data, run_len) in training_data]

    def _train_on_runs(self, model: PPO, envs_and_lens: list):
        """
        Performs training on each env from envs_and_lens. Each of the envs is
        associated with a particular cohesive run.

        Parameters
        ----------
        model: PPO
            The SB3 model object to be trained
        envs_and_lens: list
            The envs on which training is to be performed
        """
        for env, run_len in envs_and_lens:
            model.set_env(env)
            model.learn(total_timesteps=hyperparameters['dataset_traversals']*run_len)

    def _train_new_model(self, metric: str, forecaster_models: list, publish_rate: int) -> PPO:
        """
        Creates a new model, suited for predicting a specified metric
        based on specified forecasters' predictions.

        Parameters
        ----------
        metric: str
            The name of the metric whose values the new model should predict
        forecaster_models: list
            A list of the names of the forecasters on whose predictions the new ensembling
            model is to be based
        publish_rate: int
            Horizon value for the given metric

        Returns
        -------
        new_model: PPO
            The trained model
        """
        logging.info(f"Preparing to train a model for: {metric}.")

        data, total_example_num = None, 0
        while data is None:
            data, total_example_num = self._get_training_data(
                metric, forecaster_models, training_data_params['training_set_size'], publish_rate
            )
            logging.info(f"Obtained {total_example_num} datapoints suitable for training. "
                            f"Expected at least {training_data_params['training_set_size']} datapoints.")
            if data is None:
                logging.info(f"Not enough data to train the model for {metric}. "
                                "Waiting for more data.")
                self._wait_for_enough_data(training_data_params['training_set_size'], total_example_num, publish_rate)

        # Separate env for data from each run.
        envs_and_lens = self._prepare_envs(len(forecaster_models), data)

        # Create the model with the first env.
        new_model = PPO(
            'MlpPolicy',
            env=envs_and_lens[0][0],
            verbose=1,
            n_steps=hyperparameters['n_steps'],
            batch_size=hyperparameters['batch_size'],
            n_epochs=hyperparameters['n_epochs'],
            gamma=hyperparameters['gamma'],
            learning_rate=hyperparameters['learning_rate']
        )

        # Train the model on data from all runs.
        logging.info(f"Starting the training of a model for: {metric}.")
        new_model.learn(total_timesteps=hyperparameters['dataset_traversals']*envs_and_lens[0][1])
        self._train_on_runs(new_model, envs_and_lens[1:])
        logging.info(f"Training finished.")

        return new_model

    def _prepare_extended_model(self, trained_model: PPO, forecaster_models: list,
                                forecaster_positions: dict, publish_rate: int):
        """
        Creates a properly initialized instance of the ExtendedModel class.

        Parameters
        ----------
        trained_model: PPO
            Stable-Baselines 3 model object
        forecaster_models: list
            The originally ordered list of input forecasters,
            supplied in the start_ensembler message
        forecaster_positions: dict
            A mapping of forecaster names to proper positions

        Returns
        -------
        A properly initialized ExtendedModel instance
        """

        return ExtendedModel(
            trained_model,
            forecaster_models,
            forecaster_positions,
            publish_rate
        )

    def update_model(self, metric_name: str):
        """
        Updates the model currently responsible for ensembling predictions of 'metric_name'.
        This is done by gathering CONTINUAL_TRAINING_SET_SIZE fresh datapoints (it might occur,
        that slightly less than exactly CONTINUAL_TRAINING_SET_SIZE of the gathered datapoints
        have not been used for training before) and performing additional training of the model
        on the data.

        Parameters
        ----------
        metric_name: str
            The name of the metric with which the model to be trained is associated.
        """
        logging.info(f"Preparing to update the model for: {metric_name}.")

        ext_model = load_ext_model(metric_name)
        if ext_model is None:
            logging.error(f"Cannot perform continual training for {metric_name}. Could not load the persisted model.")
            return

        updated_model: PPO = ext_model.model
        forecaster_models = ext_model.input_forecasters
        publish_rate = ext_model.publish_rate

        data, total_example_num = None, 0
        while data is None:
            data, total_example_num = self._get_training_data(
                metric_name, forecaster_models, training_data_params['continual_training_set_size'], publish_rate
            )
            logging.info(f"Obtained {total_example_num} datapoints suitable for continual training. "
                            f"Expected at least {training_data_params['continual_training_set_size']} datapoints.")
            if data is None:
                # Pause the continual training job until we start to perform this
                # round of continual training.
                logging.info(f"Not enough data to perform additional training of the model for {metric_name}. "
                                "Waiting for more data and halting further continual training for now.")
                self.scheduler.pause_job(metric_name, 'default')
                self._wait_for_enough_data(training_data_params['continual_training_set_size'], total_example_num, publish_rate)
            
        # Resume the job that was potentially paused before.
        #TODO tu też coś będzie z tym mutexem chyba
        self.scheduler.resume_job(metric_name, 'default')

        # Separate env for data from each run.
        envs_and_lens = self._prepare_envs(len(forecaster_models), data)

        # Perform additional training of the model on fresh data from all runs.
        logging.info(f"Starting continual training of the model for: {metric_name}.")
        self._train_on_runs(updated_model, envs_and_lens)
        logging.info("Continual training finished.")       

        # We only update the Stable-Baselines3 model object to preserve the
        # state, which is independent of the inner model/neural network.
        #TODO tutaj może mutex i w tym drugim miejscu gdzie jest przypisanie nowego modelu też.
        # No i też to sprawdzenie, czy w międzyczasie (w trakcie dotrynowywania) nie zakończyło
        # się ponowne trenowanie z rozkazu, żeby nie popsuć (bo to napotkałem). Czyli może anulowanie
        # wszystkich jobów przed rozpoczęciem trenowania na nowo (albo przed ustawieniem nowego ExtendedModel)
        # Albo tutaj podmieniać tylko jesli if forecasters match.
        self.models[metric_name].model = updated_model

        # Only persist the new internal Stable-Baselines3 model object.
        save_model(metric_name, updated_model)


    def _calculate_model_update_interval(self, horizon: int) -> int:
        """
        Calculates how often the newly created model should be updated,
        based on the publish rate of new data.

        Parameters
        ----------
        horizon: int
            New metric data publish rate

        Returns
        -------
        Model update interval (in seconds)
        """

        # More frequent version for manual tests:
        # return (horizon // MS_IN_S)

        return (horizon // MS_IN_S) * int(training_data_params['continual_training_set_size'] * training_data_params['fresh_data_ratio'])

    def _construct_model(self, metric_info: dict, forecaster_models: list, forecaster_positions: dict):
        """
        Trains a new model for the metric from metric_info and saves it.

        Parameters
        ----------
        metric_info
            Info about the metric from start_ensembler order
        forecaster_models: list
            The originally ordered list of input forecasters,
            supplied in the start_ensembler message
        forecaster_positions: dict
            A mapping of forecaster names to proper positions
        """
        metric_name = metric_info["metric"]

        # We prevent the continual training job to be run in the middle
        # of new training.
        self._remove_continual_training_job(metric_name)

        publish_rate = metric_info["publish_rate"]
        model = self._prepare_extended_model(
            self._train_new_model(metric_name, forecaster_models, publish_rate),
            forecaster_models,
            forecaster_positions,
            publish_rate
        )
        self.models[metric_name] = model #TODO lock na models tu i przy updacie (i przy predykowaniu)?
        save_ext_model(metric_name, model)
        
        # A scheduler job responsible for continual training of the
        # newly created model is added.
        # The job is scheduled to run periodically every N seconds,
        # where N is the time during which CONTINUAL_TRAINING_SET_SIZE
        # new datapoints are expected to be produced.
        self._add_continual_training_job(metric_name, publish_rate)

    def train_new_models(self, start_ensembler: dict):
        """
        Trains new models, compliant with the order
        gathered from the queue and stores them in
        the models dict.

        Parameters
        ----------
        start_ensembler: dict
            JSON data that defines the new ensembling policy
        """
        logging.info("Preparing to train new models.")

        metrics_info: list = start_ensembler["metrics"]
        forecaster_models: list = start_ensembler["models"]

        # The position mapping is the same for the whole batch of new models.
        forecaster_positions = self._prepare_forecaster_positions(forecaster_models)

        for metric_info in metrics_info:
            # Perform the model trainings concurrently.
            # A job scheduled this way is run once, immediately.
            self.scheduler.add_job(
                func=self._construct_model,
                args=[metric_info, forecaster_models, forecaster_positions]
            )

    def _calculate_prediction(self, weights, forecasts: np.ndarray) -> float:
        """
        Applies the weights produced by the model, that is,
        calculates the weighted mean on the received forecasts
        based on the produced weights.

        Parameters
        ----------
        weights
            The weights produced by the model
        forecasts: np.ndarray
            The forecasters' predictions received from the PO,
            rewritten into an array in a proper order.

        Returns
        -------
        prediction: float
            The ensembled prediction
        """

        weight_sum: float = np.sum(weights)

        if weight_sum > 0:
            effective_weights = weights / weight_sum
            logging.debug("Effective weights (after normalization):")
            logging.debug(effective_weights)
            return np.sum(effective_weights * forecasts)
        else:
            logging.info("All of the produced weights were equal to 0. Calculating arithmetic average on predictions.")
            return np.mean(forecasts)

    def _order_predictions(self, predictions: dict, ext_model: ExtendedModel) -> np.ndarray:
        """
        Rewrites the received forecaster predictions, which are unordered,
        to an array in the order required by the specified model.

        Parameters
        ----------
        predictions: dict
            The received "forecaster name" -> "forecaster prediction" mapping
        ext_model: ExtendedModel
            The model selected for current ensembling

        Returns
        -------
        forecasts: np.ndarray
            Properly ordered input forecasts
        """
        forecasts = np.zeros(len(ext_model.forecaster_positions))
        for key, value in predictions.items():
            forecasts[ext_model.forecaster_positions[key]] = value

        return forecasts

    def _compute_hole_fill_value(self, forecasts: np.ndarray) -> float:
        """
        Calculates the value with which nulls/nans should be replaced
        in the input forecasts array.

        Parameters
        ----------
        forecasts: np.ndarray
            Input forecasts

        Returns
        -------
        The float value to fill the holes with
        """
        forecaster_num = len(forecasts)
        forecasters_online: int = 0
        forecasters_sum: float = 0.

        for i in range(forecaster_num):
            if not np.isnan(forecasts[i]):
                forecasters_online += 1
                forecasters_sum += forecasts[i]

        if forecasters_online == 0:
            logging.info("All of the received predictions were None. Producing a random prediction.")
            return np.random.random_sample() * training_data_params['max_prediction_value']
        else:
            return forecasters_sum / forecasters_online

    def _fill_input_forecaster_holes(self, forecasts: np.ndarray):
        """
        Fills potential "holes" (nulls) in the input forecasts array.
        Modifies the input array (does not create a new one).

        Parameters
        ----------
        forecasts: np.ndarray
            Input forecasts
        """
        if np.any(np.isnan(forecasts)):
            filler_value = self._compute_hole_fill_value(forecasts)
            forecasts[np.isnan(forecasts)] = filler_value

    def _consult_forecasters(self, forecast_info: ForecastInfo) -> bool:
        """
        Checks whether the forecasters observed in input forecast list are the same
        forecasters with which the model for the given metric is compatible.

        Parameters
        ----------
        forecast_info: ForecastInfo
            Object with info gathered from the PO request

        Returns
        -------
        True iff the forecasters match
        """
        expected = set(self.models[forecast_info.metric].input_forecasters)
        actual = set(forecast_info.predictions.keys())

        logging.debug("Expected input forecasters:")
        logging.debug(expected)

        logging.debug("Actual input forecasters:")
        logging.debug(actual)

        return actual == expected

    def predict(self, forecast_info: ForecastInfo) -> float:
        """
        Produces an ensemble of the predictions received from PO.

        Parameters
        ----------
        forecast_info: ForecastInfo
            An object that contains the predictions from the forecasters
            and the forecasted metric's name.

        Returns
        -------
        prediction: float
            The ensembled prediction
        """
        logging.info(f"Ensembling `{forecast_info.metric}` predictions.")

        # We choose the model that is trained for ensembling the specified metric's predictions.
        ext_model = self.models.get(forecast_info.metric)

        # An adequate model does not exist.
        if ext_model is None:
            return None

        # Input forecasters are not the same as the ones on which the given model is based.
        if not self._consult_forecasters(forecast_info):
            return None

        ext_model.update_state()
        
        forecasts = self._order_predictions(forecast_info.predictions, ext_model)

        self._fill_input_forecaster_holes(forecasts)
        logging.debug("Input forecasts after filling holes:")
        logging.debug(forecasts)

        weights = ext_model.predict()
        logging.debug("Weights produced by the model:")
        logging.debug(weights)

        prediction = self._calculate_prediction(weights, forecasts)
        
        ext_model.previous_prediction = prediction

        return prediction
