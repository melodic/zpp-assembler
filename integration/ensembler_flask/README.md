## Ensembling component

### Requirements
- Docker

### How to prepare the component
- Prepare the `properties.yml` file
    - The default file is the `config/properties.yml` file. You can modify this one or store a modified version somewhere else. If you want to avoid rebuilding the image every time after changing the properties, it is important to mount the appropriate properties file on the host machine to the `/ensembler/config/properties.yml` file in the container's filesystem when running the container (using the Docker volumes functionality).
- Prepare the `server_params.env` file
    - The default file is the `config/server_params.env` file. You need to set the port on which the app is to be exposed.
- Build the component's image as you would build any other Docker image, that is using e.g.: `docker build -t ensembler .`

### How to run the component
Assuming that you:
- run the component from this directory
- have named the image `ensembler`
- have configured the `properties.yml` file properly
- have configured the `server_params.env` file properly

you can run the component e.g. as follows:
```
docker run \
  --env-file <path_to_your_server_params_file> \
  -p <host_machine_port_on_which_ensembler_is_to_be_accessible>:<ensembler_port_you've_set_in_the_server_params_file> \
  -v <dir_in_which_you_wish_to_persist_ensembling_models>:/ensembler/saved_models \
  -v <dir_in_which_you_wish_to_save_logs>:/ensembler/logs \
  -v <path_to_your_properties_file>:/ensembler/config/properties.yml \
  ensembler
```
Notice:
- the env-file with environment variables related to the server
- the following mounts:
    - `properties.yml` file
    - `saved_models` directory (if you wish to persist the models between container runs)
    - `logs` directory (if you wish to access the logs after stopping the container)

### How to interact with the component
The component handles two types of input:
- POST requests (with properly formatted JSON body containing forecasters' predictions) sent to the server
    - **Response:** JSON with an ensembled prediction
- `start_ensembler` STOMP messages sent to the proper ActiveMQ topic
    - After consuming such a message from the topic, training of adequate models is initialized.
