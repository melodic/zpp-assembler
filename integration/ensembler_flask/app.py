from flask import Flask, request
import time
import logging
from ensembler_app.Predictor import Predictor
from ensembler_app.ForecastInfo import ForecastInfo
from ensembler_app.directories import PROPERTIES_PATH
from ensembler_app.utils import parse
from app_init_utils import config_logging, connect_to_influx, connect_to_mq

NS_IN_MS = 10**6

p = parse(PROPERTIES_PATH)
influx_conf = p['components']['influxdb']
stomp_conf = p['components']['stomp']
log_settings = p['logging']

config_logging(log_settings)

influxdb_client = connect_to_influx(influx_conf)

logging.info("Initializing the Predictor.")
predictor = Predictor(influxdb_client)
logging.info("Predictor successfully initialized.")

connect_to_mq(stomp_conf, predictor)

logging.info("Initializing Flask app.")
app = Flask(__name__)
logging.info("Successfully initialized the Flask app. Waiting for PO requests.")

@app.route(p['api-endpoint'], methods=['POST'])
def predict():
    logging.info("Received an ensembling request.")
    req_json = request.json
    logging.debug(req_json)

    forecast_info = ForecastInfo(req_json)

    ensembled_prediction = predictor.predict(forecast_info)

    if ensembled_prediction is None:
        logging.error("None of the currently trained models is suitable for performing the requested ensembling.")
        return {
            "error": p['errors']['no-adequate-model-msg']
        }, p['errors']['no-adequate-model-code']

    logging.info(f"Ensembled prediction successfully produced: {ensembled_prediction}")

    resp = {
        "ensembledValue": ensembled_prediction,
        "timestamp": (time.time_ns() // NS_IN_MS) // 10**3,
        "predictionTime": forecast_info.prediction_time
    }
    logging.info("Sending response to PO.")
    logging.debug(resp)

    return resp