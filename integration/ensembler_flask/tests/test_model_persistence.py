import pytest
import os
from stable_baselines3 import PPO
import json

from tests.common import TRAINED_MODEL_DIR, delete_model

from ensembler_app import model_persistence
from ensembler_app.ExtendedModel import ExtendedModel
from ensembler_app.directories import SAVED_MODELS_DIR

class TestModelPersistence:
    test_model = PPO.load(TRAINED_MODEL_DIR)
    input_forecasters = ["test1", "test2", "test3", "test4"]

    def create_forecaster_positions(self, forecaster_models: list):
        return dict(zip(forecaster_models, range(len(forecaster_models))))

    def create_ext_model(self):
        forecaster_models = self.input_forecasters
        positions = self.create_forecaster_positions(forecaster_models)
        ext_model = ExtendedModel(self.test_model, forecaster_models, positions, 40000)
        return ext_model
  
    @pytest.mark.parametrize("metric", ["NewMetric"])
    def test_save_model(self, metric: str):
        os.mkdir(SAVED_MODELS_DIR) # make sure the saved_models dir exists

        model_dir_path, model_path, _ = delete_model(metric)
        try:
            assert not os.path.exists(model_dir_path)   # make sure the model has been deleted
            os.mkdir(model_dir_path)    # metric model dir needs to exist
            model_persistence.save_model(metric, self.test_model)
            assert os.path.exists(model_path)       # make sure the model has been saved
        finally:
            delete_model(metric)

    # saves a mock extended model as json and PPO model files in the saved_models directory
    def save_ext_model(self, metric: str):
        model_dir_path, model_path, json_path = delete_model(metric)
        assert not os.path.exists(model_dir_path)   #make sure the model has been deleted
        ext_m = self.create_ext_model()
        model_persistence.save_ext_model(metric, ext_m)
        assert os.path.exists(model_path)       # make sure the model has been saved
        assert os.path.exists(json_path)        # make sure json file has been created    
        with open(json_path, "r") as model_json:
            data = json.load(model_json)
            assert data["input_forecasters"] == ext_m.input_forecasters
            assert data["forecaster_positions"] == ext_m.forecaster_positions
            assert data["publish_rate"] == ext_m.publish_rate 

    @pytest.mark.parametrize("metric", ["NewMetric"])
    def test_save_ext_model(self, metric: str):
        try:
            self.save_ext_model(metric)
        finally:
            delete_model(metric)
    
    @pytest.mark.parametrize("metric", ["NewMetric"])
    def test_load_ext_model_empty(self, metric : str):
        delete_model(metric)
        extm = model_persistence.load_ext_model(metric)
        assert extm is None
    
    @pytest.mark.parametrize("metric", ["NewMetric"])
    def test_load_ext_model(self, metric : str):
        try:
            self.save_ext_model(metric)
            extm = model_persistence.load_ext_model(metric)
            assert extm is not None
            prediction = extm.predict()
            assert prediction is not None
        finally:
            delete_model(metric)
            os.rmdir(SAVED_MODELS_DIR)
