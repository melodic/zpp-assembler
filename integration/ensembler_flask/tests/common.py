import os

from ensembler_app.directories import ROOT_DIR, SAVED_MODELS_DIR

TRAINED_MODEL_DIR = os.path.join(ROOT_DIR, 'tests', 'ppo_trained.zip')

def delete_model(metric : str):     
  model_dir_path = os.path.join(SAVED_MODELS_DIR, metric)
  model_path = os.path.join(model_dir_path, f"{metric}.zip")
  json_path = os.path.join(model_dir_path, f"{metric}.json")
  if os.path.exists(model_dir_path):
    if os.path.exists(model_path):
      os.remove(model_path)
    if os.path.exists(json_path):
        os.remove(json_path)
    os.rmdir(model_dir_path)
  return (model_dir_path, model_path, json_path)