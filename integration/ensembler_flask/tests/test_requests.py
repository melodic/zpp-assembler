import random
import requests
import pytest
import stomp
import os
import yaml
from time import sleep

from dotenv import dotenv_values

"""
Tests validating requests sent to the component, as well as training orders.

IMPORTANT: The component needs to be running on $ENSEMBLER_HOST_IP:$PORT, where
ENSEMBLER_HOST_IP is an environment variable (the default value is 127.0.0.1 (localhost),
which is the usual host address) and PORT is the port in the
server_params.env file, provided for the running component instance.

Moreover, the assumption is made that the component has been run using the
ensembler_flask/tests/config configuration files and not the production ones from 
ensembler_flask/config.
"""

TESTS_DIR = os.path.dirname(os.path.abspath(__file__))
CONFIG_DIR = os.path.join(TESTS_DIR, 'config')

server_config = dotenv_values(os.path.join(CONFIG_DIR, "server_params.env"))
properties = {}
with open(os.path.join(CONFIG_DIR, 'properties.yml'), "r") as stream:
    properties = yaml.safe_load(stream)

GITLAB_ACTIVEMQ_HOST_IP = os.environ.get('GITLAB_ACTIVEMQ_HOST_IP')

RESPONSE_CODE = 200
NO_MODEL_CODE = properties["errors"]["no-adequate-model-code"] # 512
HOST_IP = os.environ.get('ENSEMBLER_HOST_IP', '127.0.0.1')
PORT = server_config["PORT"]
USERNAME = properties["components"]["stomp"]["username"]
PASSWORD = properties["components"]["stomp"]["password"]
STOMP_PORT = properties["components"]["stomp"]["port"]
MQ_TOPIC = properties["components"]["stomp"]["destination"]
ROUTE = "http://" + HOST_IP + ":" + PORT + properties['api-endpoint']


METRIC1 = "MinCPULoad"
METRIC2 = "MaxCPULoad"
TRAIN_CPU_MSG = \
    '{ \
        "metrics": [ \
            { \
                "metric": "' + METRIC1 + '", \
                "level":3, \
                "publish_rate":30000 \
            }, \
            { \
                "metric": "' + METRIC2 + '", \
                "level":3, \
                "publish_rate":30000 \
            } \
        ], \
        "models": ["cnn","tft","nbeats"] \
    }'

class TestRequests:
    max_forecast = 200
    train_timeout = 500  # max time in seconds given for the NN to complete its training
    conn = {}

    if GITLAB_ACTIVEMQ_HOST_IP is None:
        conn = stomp.Connection([(HOST_IP, STOMP_PORT)])
    else:
        conn = stomp.Connection([(GITLAB_ACTIVEMQ_HOST_IP, STOMP_PORT)])
    conn.connect(USERNAME, PASSWORD, wait=True)

    def create_json(self, metric):
        json={
            "method": "RL",
            "metric": metric,
            "predictionTime": 0,
            "predictionsToEnsemble": {
                "tft": random.randrange(0, self.max_forecast),
                "nbeats": random.randrange(0, self.max_forecast),
                "cnn": random.randrange(0, self.max_forecast)
            }
        }
        return json

    def create_json_single_NaN(self, metric):
        json={
            "method": "RL",
            "metric": metric,
            "predictionTime": 0,
            "predictionsToEnsemble": {
                "tft": random.randrange(0, self.max_forecast),
                "cnn": random.randrange(0, self.max_forecast),
                "nbeats": None
            }
        }
        return json

    def create_json_NaN(self, metric):
        json={
            "method": "RL",
            "metric": metric,
            "predictionTime": 0,
            "predictionsToEnsemble": {
                "tft": None,
                "nbeats": None,
                "cnn": None
            }
        }
        return json
  
    def create_json_wrong(self, metric):
        json={
            "method": "RL",
            "metric": metric,
            "predictionTime": 0,
            "predictionsToEnsemble": {
                "tft": 10,
                "not a name": 0,
                "cnn": 1
            }
        }
        return json

    def post_request(
            self,
            metric,
            singleNaN : bool = False,
            NaN : bool = False,
            wrong : bool = False
        ):
        if wrong:
            r = requests.post(ROUTE, json=self.create_json_wrong(metric))
        elif singleNaN:
            r = requests.post(ROUTE, json=self.create_json_single_NaN(metric))
        elif NaN:
            r = requests.post(ROUTE, json=self.create_json_NaN(metric))
        else:
            r = requests.post(ROUTE, json=self.create_json(metric))
        return r

    def send_train_request(self):
        self.conn.send(MQ_TOPIC, TRAIN_CPU_MSG)

    def test_response_untrained(self):
        r = self.post_request(METRIC1)
        print(f"Status Code: {r.status_code}, Response: {r.json()}")
        print(r.json())
        assert r.status_code == NO_MODEL_CODE
        r = self.post_request(METRIC2)
        print(f"Status Code: {r.status_code}, Response: {r.json()}")
        print(r.json())
        assert r.status_code == NO_MODEL_CODE

    def ensure_trained(self, metric):
        code = self.post_request(metric).status_code
        i = 0
        while code == NO_MODEL_CODE:
            r = self.post_request(metric)
            code = r.status_code
            print(i, r.json())
            sleep(1.0)  # sleep for 1.0 sec
            assert(i < self.train_timeout)
            i = i+1
        assert code == RESPONSE_CODE

    # sends a request to train two new models and waits until
    # they have finished training
    def order_and_ensure_training(self):
        self.send_train_request()
        self.ensure_trained(METRIC1)
        self.ensure_trained(METRIC2)
  
    def test_many_requests(self):
        self.order_and_ensure_training()
        # we have received a response code with the first ensembled prediction,
        # indicating that the training has ended
        for i in range (100):
            metric = random.choice([METRIC1, METRIC2])
            r = self.post_request(metric)
            print(i, r.json())
            assert r.status_code == RESPONSE_CODE
        for i in range (100):
            metric = random.choice([METRIC1, METRIC2])
            r = self.post_request(metric, singleNaN = True) # single NaN value in forecaster predictions
            print(i, r.json())
            assert r.status_code == RESPONSE_CODE
        for i in range (100):
            metric = random.choice([METRIC1, METRIC2])
            r = self.post_request(metric, wrong = True) # inexistent forecaster name
            print(i, r.json())
            assert r.status_code == NO_MODEL_CODE
        for i in range (100):
            metric = random.choice([METRIC1, METRIC2])
            r = self.post_request(metric, NaN = True) # NaN values in forecaster predictions
            print(i, r.json())
            assert r.status_code == RESPONSE_CODE
