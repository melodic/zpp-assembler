import numpy as np
import pandas as pd
import numpy as np
import pytest
from influxdb import InfluxDBClient
from stable_baselines3 import PPO

from ensembler_app.Predictor import Predictor
from ensembler_app.ExtendedModel import ExtendedModel
from tests.common import TRAINED_MODEL_DIR

from ensembler_app.directories import PROPERTIES_PATH
from ensembler_app.utils import parse

def permutations(l):
    if len(l) == 0:
        return []
    if len(l) == 1:
        return [l]
    ret = []
    for i in range(len(l)):
        rest = l[:i] + l[i+1:]
        for p in permutations(rest):
            ret.append([l[i]] + p)
    return ret

class TestPredictor:
    test_model = PPO.load(TRAINED_MODEL_DIR)

    def arr_to_series(self, arr):
        return pd.Series(arr, index=np.arange(len(arr)))

    @pytest.mark.parametrize("forecasters,predictions", [(["a","b","c"],[6,7,8])])
    def test_prepare_forecaster_positions(self, forecasters: list, predictions: list):
        predictor = Predictor(InfluxDBClient)
        forecaster_positions = predictor._prepare_forecaster_positions(forecasters)
        print(forecaster_positions)
        extm = ExtendedModel(self.test_model, forecasters, forecaster_positions, publish_rate=1)
        
        pred_dict = dict(zip(forecasters, predictions))
        pred_list = list(pred_dict.items())
        print("dict ", pred_dict)
        print("list ", pred_list)
        for p in permutations(pred_list):
            d = dict((x, y) for x, y in p)       # permutated, but same dictionary
            print(d)
            ens_forecasts = predictor._order_predictions(d, extm)
            print(ens_forecasts)
            assert (ens_forecasts == predictions).all()

    @pytest.mark.parametrize("tailsize", [0,1,2,3,9,10])
    def test_trim_data(self, tailsize):
        a = np.array(range(10)).astype(float)
        b = np.zeros(10).astype(float)
        c = np.array(range(10)).astype(float)
        for i in range (tailsize):
            a[i] = np.nan
            b[i] = np.nan
            c[i] = np.nan
        data = {"a": a, "b": b, "c": c}
        df = pd.DataFrame(data)
        print("before trim: ", df)
        print("size:", df.size)
        predictor = Predictor(InfluxDBClient)
        trimmed = predictor._trim_data(df)
        # assert trimmed is not None
        print("trimmed:\n",trimmed)
        print("size: ", trimmed.size)
        assert trimmed.size == df.size - tailsize * 3
    @pytest.mark.parametrize("weights, forecasts", 
                            [(np.random.rand(30), np.random.rand(30)),
                             (np.arange(20), np.random.randint(low=1,high=100, size=20))])
    def test_calculate_prediction(self, weights, forecasts: np.ndarray):
        predictor = Predictor(InfluxDBClient)
        pred = predictor._calculate_prediction(weights, forecasts)
        effective_weights = weights / np.sum(weights)
        assert pred == np.sum(effective_weights * forecasts)
    
    @pytest.mark.parametrize("forecasts", [np.random.rand(100), np.zeros(100)])
    def test_calculate_prediction_zeros(self, forecasts: np.ndarray):
        predictor = Predictor(InfluxDBClient)
        weights = np.zeros(100)
        pred = predictor._calculate_prediction(weights, forecasts)
        assert pred == np.mean(forecasts)
    
    @pytest.mark.parametrize("forecasts", [
                                            np.array([np.nan,np.nan,np.nan,np.nan]), 
                                            np.array([np.nan]),
                                            np.arange(15, dtype=float),
                                            np.zeros(20),
                                            np.array([1.0,np.nan,np.nan,2.0,3.0, np.nan])
                                            ])
    def test_fill_input_forecaster_holes(self, forecasts):
        predictor = Predictor(InfluxDBClient)
        predictor._fill_input_forecaster_holes(forecasts)
        assert forecasts is not None
        print(forecasts)
        assert not np.isnan(forecasts.all())
    
    @pytest.mark.parametrize("publish_rate", [1,10,123])
    def test_split_data_into_cohesive_runs(self, publish_rate: int):
        predictor = Predictor(InfluxDBClient)

        training_data_params = parse(PROPERTIES_PATH)['training-data-params']
        ns_in_ms = 10**6
        splitting_diff = training_data_params['run_break_len_factor'] * publish_rate * ns_in_ms
        assert(splitting_diff >= 1)

        t1 = np.arange(3, (splitting_diff/2)*7, splitting_diff/2, dtype=int)
        t2 = np.arange(t1[-1]+splitting_diff+1, t1[-1]+splitting_diff*11+1, splitting_diff, dtype=int)
        t3 = np.array([t2[-1] + 2*splitting_diff])
        expected = [t1,t2,t3]
        timestamps = np.concatenate((t1,t2,t3),axis=0)
        data = pd.DataFrame({"time": timestamps})

        df_list = predictor._split_data_into_cohesive_runs(data, publish_rate)

        assert(len(df_list) == 3)
        for i in range (3):
            assert((df_list[i].loc[:,"time"] == expected[i]).all())