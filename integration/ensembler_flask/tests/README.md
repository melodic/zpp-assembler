# Tests

NOTICE: Disregard the `Dockerfile` in this directory. It is used solely for running the
requests test in GitLab CI.

### Test descriptions
- `test_model_persistence.py`: model persistence unit tests
- `test_predictor.py`: unit tests of various utility methods crucial for the ensembler
- `test_requests.py`: integration tests

### Before tests
Run the below command in the `ensembler_flask` directory:
```
poetry install
```

and set up proper configuration in all files in `config`.

### How to run a unit test
```
UNIT_TESTS_MODE='True' poetry run pytest <test_file>
```

### How to run the integration tests
```
ENSEMBLER_HOST_IP=<ip_or_hostname_of_ensembler_host_machine> poetry run pytest test_requests.py
```
The default `ENSEMBLER_HOST_IP` value is `127.0.0.1` (localhost).
