import logging
import sys
import os

import stomp

from influxdb import InfluxDBClient

from ensembler_app.directories import LOGS_DIR
from ensembler_app.EnsembleSettingsListener import EnsembleSettingsListener

def log_exception(e_type, e_value, e_traceback):
    """
    Taken from: https://stackoverflow.com/questions/6234405/logging-uncaught-exceptions-in-python/16993115#16993115
    
    Used to log uncaught exceptions to the log file.
    """

    # Allow ending the program with Ctrl-C.
    if issubclass(e_type, KeyboardInterrupt):
        sys.__excepthook__(e_type, e_value, e_traceback)
        return

    logging.error("Uncaught exception", exc_info=(e_type, e_value, e_traceback))

def config_logging(log_settings):
    sys.excepthook = log_exception

    if not os.path.exists(LOGS_DIR):
        os.mkdir(LOGS_DIR)

    logging.basicConfig(
        level=logging.getLevelName(log_settings['level']),
        datefmt=log_settings['date-format'],
        format=log_settings['format'],
        handlers=[
            logging.FileHandler(os.path.join(LOGS_DIR, log_settings['filename'])),
            logging.StreamHandler(sys.stdout)
        ]
    )

def connect_to_influx(influx_conf) -> InfluxDBClient:
    logging.info("Connecting to InfluxDB.")
    influxdb_client = InfluxDBClient(influx_conf['hostname'], influx_conf['port'], influx_conf['username'], influx_conf['password'], retries=0)
    logging.info("Successfully connected to InfluxDB.")
    return influxdb_client

def connect_to_mq(stomp_conf, predictor):
    logging.info("Connecting to ActiveMQ.")
    conn = stomp.Connection(
        [(stomp_conf['hostname'], stomp_conf['port'])],
        reconnect_attempts_max=stomp_conf['reconnect_attempts_max'],
        reconnect_sleep_initial=stomp_conf['reconnect_sleep_initial']
    )
    conn.set_listener('', EnsembleSettingsListener(predictor))
    conn.connect(username=stomp_conf['username'], passcode=stomp_conf['password'], wait=True)
    conn.subscribe(destination=stomp_conf['destination'], id=stomp_conf['id'])
    logging.info("Successfully connected to ActiveMQ.")