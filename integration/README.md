# Interactive ensembling component and test environment

This directory contains both the ensembling component (`ensembler_flask`) and the testing environment (`system-setup`) that contains all services necessary for the ensembler to work (ActiveMQ and Influx packed into a docker-compose, along with the ensembling component itself).
