python3 -m stomp -H localhost -P 61613


> send /topic/start_ensembler {"metrics":[{"metric":"MaxCPULoad","level":3,"publish_rate":50000}, {"metric":"MinCPULoad","level":3,"publish_rate":50000}],"models":["cnn","tft","nbeats"]}

{
    "metrics":[
        {
            "metric":"MaxCPULoad",
            "level":3,
            "publish_rate":50000
        },
        {
            "metric":"MinCPULoad",
            "level":3,
            "publish_rate":50000
        }
    ],
    "models":[
        "cnn",
        "tft",
        "nbeats"
    ]
}

> send /topic/start_ensembler {"metrics":[{"metric":"MinimumCores","level":3,"publish_rate":30000}],"models":["cnn","nbeats"]}

{
    "metrics":[
        {
            "metric":"MinimumCores",
            "level":3,
            "publish_rate":30000
        }
    ],
    "models":[
        "cnn",
        "nbeats"
    ]
}

> send /topic/start_ensembler {"metrics":[{"metric":"MaximumCores","level":3,"publish_rate":30000},{"metric":"MinimumCores","level":3,"publish_rate":30000}],"models":["cnn","nbeats","tft"]}

{
    "metrics":[
        {
            "metric":"MaximumCores",
            "level":3,
            "publish_rate":30000
        },
        {
            "metric":"MinimumCores",
            "level":3,
            "publish_rate":30000
        }
    ],
    "models":[
        "cnn",
        "nbeats",
        "tft"
    ]
}

> send /topic/start_ensembler {"metrics":[{"metric":"MinCPULoad","level":3,"publish_rate":30000}],"models":["cnn","tft","nbeats"]}

> send /topic/start_ensembler {"metrics":[{"metric":"MinCPULoad","level":3,"publish_rate":30000}, {"metric":"MaxCPULoad","level":3,"publish_rate":30000}],"models":["cnn","tft","nbeats"]}
