#!/bin/sh

if [ "$#" -ne 2 ]; then
  echo "Usage: ./fill_db.sh <influx_host:influx_port> <sample_data_dir>"
  exit 1
fi

influx_host="$1"
sample_data_dir="$2"

pip3 install -r ./csv-to-influxdb/requirements.txt

for file in ${sample_data_dir}/melodic_ui/*; do
  python3 ./csv-to-influxdb/csv-to-influxdb.py -i "$file" -s "$influx_host" -u admin -p admin1234 --dbname melodic_ui -m name -tc "time" --tagcolumns countryCode,ipAddress,level,producer
done

for file in ${sample_data_dir}/morphemic/*; do
  python3 ./csv-to-influxdb/csv-to-influxdb.py -i "$file" -s "$influx_host" -u admin -p admin1234 --dbname morphemic -m name -tc "time" --tagcolumns '' --fieldcolumns cnn_left_conf,cnn_probability,cnn_right_conf,cnn_value,ensembled_left_conf,ensembled_probability,ensembled_right_conf,ensembled_value,nbeats_left_conf,nbeats_probability,nbeats_right_conf,nbeats_value,tft_left_conf,tft_probability,tft_right_conf,tft_value
done
