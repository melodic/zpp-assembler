# Test environment

### Requirements
- Docker Compose

### How to run the environment
Run the following command in this directory:
```
docker-compose up
```
Remember to configure the `../ensembler_flask/tests/config/properties.yml` and `../ensembler_flask/tests/config/flask_params.env` files properly, as well as set the port mapping in the `docker-compose.yml` file.

### How to setup the environment (only after the first startup)
- Enter the `influxdb` directory.
- Run the `fill_db.sh` script with proper arguments, that is:
  ```
  ./fill_db.sh <docker-host-machine-ip-address>:8086 <sample_data_dir>
  ```
  It will fill the InfluxDB database with sample data from `<sample_data_dir>`. The script uses a modified version of a simple tool for uploading csv data to Influx (found in the `csv-to-influxdb` directory; original version taken from (https://github.com/fabio-miranda/csv-to-influxdb) ).
