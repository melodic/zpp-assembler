# ZPP Ensembler GitLab CI config

variables:
  SYSTEM_SETUP_DIR: $CI_PROJECT_DIR/integration/system-setup
  ENSEMBLER_DIR: $CI_PROJECT_DIR/integration/ensembler_flask
  DOCKER_DIND_IMAGE: "docker:19.03.12"
  DOCKER_DIND_SERVICE: "$DOCKER_DIND_IMAGE-dind"
  CI_REGISTY: "gitlab.ow2.org:4567"
  CONTAINER_BASE_TAG: $CI_REGISTRY_IMAGE/ensembler
  CONTAINER_RELEASE_IMAGE: $CONTAINER_BASE_TAG:$CI_COMMIT_BRANCH

stages:
  - unit-tests
  - build
  - integration-tests
  - deploy

# Perform unit tests
unit-tests:ensembler:
  stage: unit-tests
  image: python:3.8-slim-buster
  only:
    - integration
  script:
    - cd $ENSEMBLER_DIR
    - pip install --no-cache-dir --upgrade pip
    - pip install --no-cache-dir poetry
    - poetry install
    - UNIT_TESTS_MODE='True' poetry run pytest tests/test_predictor.py
    - UNIT_TESTS_MODE='True' poetry run pytest tests/test_model_persistence.py

# Build Docker image
build:ensembler:      
  stage: build
  image: $DOCKER_DIND_IMAGE
  only:
    - integration
  services:
    - $DOCKER_DIND_SERVICE
  script:
    - cd $ENSEMBLER_DIR
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $CONTAINER_RELEASE_IMAGE || true   # Attempt to pull previous image as a cache for the below build.
    - docker build --cache-from $CONTAINER_RELEASE_IMAGE -t ensembler .
    - docker image ls
    - cd $CI_PROJECT_DIR
    - mkdir image
    - docker save ensembler > image/ensembler.tar  # Save the image as an artifact for further stages
  artifacts:
    paths:
      - image

# Test requests sent to endembler
integration-tests:ensembler:
  stage: integration-tests
  image: $DOCKER_DIND_IMAGE
  only:
    - integration
  services:
    - $DOCKER_DIND_SERVICE
    - name: rmohr/activemq:5.15.9
      alias: activemq
    - name: influxdb:1.8.4
      alias: influxdb
      variables:
        INFLUXDB_ADMIN_USER: admin
        INFLUXDB_ADMIN_PASSWORD: admin1234
  script:
    # Install essential packages
    - apk add --update --no-cache curl python3 py3-pip alpine-sdk
    # Create appropriate databases in InfluxDB
    - 'curl --user admin:admin1234 -XPOST "http://influxdb:8086/query" --data-urlencode "q=CREATE DATABASE melodic_ui"'
    - 'curl --user admin:admin1234 -XPOST "http://influxdb:8086/query" --data-urlencode "q=CREATE DATABASE morphemic"'
    # Load the artifact image
    - docker load -i image/ensembler.tar
    # Start the ensembler
    - docker run 
        -d 
        --env-file $ENSEMBLER_DIR/tests/config/server_params.env 
        --add-host=activemq:$(getent hosts activemq  | awk '{ print $1 }') 
        --add-host=influxdb:$(getent hosts influxdb  | awk '{ print $1 }') 
        -v $ENSEMBLER_DIR/tests/config/properties.yml:/ensembler/config/properties.yml 
        --name ensembler 
        ensembler
    - cd $SYSTEM_SETUP_DIR/influxdb
    - chmod +x ./fill_db.sh
    # Upload test data to InfluxDB
    - ./fill_db.sh 'influxdb:8086' 'sample_data_separate_runs'
    - cd $ENSEMBLER_DIR
    # Build an image for running the requests test
    - docker build -f ./tests/Dockerfile -t tests_image .
    # Run the requests test
    - docker run 
        --env ENSEMBLER_HOST_IP=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ensembler) 
        --env GITLAB_ACTIVEMQ_HOST_IP=$(getent hosts activemq  | awk '{ print $1 }') 
        tests_image
    - docker stop ensembler

# Push the built and tested image
deploy:ensembler:
  stage: deploy
  image: $DOCKER_DIND_IMAGE
  only:
    - integration
  services:
    - $DOCKER_DIND_SERVICE
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker load -i image/ensembler.tar   # Load the artifact image
    - docker tag ensembler $CONTAINER_RELEASE_IMAGE
    - docker push $CONTAINER_RELEASE_IMAGE
